package com.example.jiji_manage1;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.posapi.PosApi;
import android.posapi.PrintQueue;
import android.widget.Toast;
import android.posapi.PrintQueue.OnPrintListener;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

class PrinterClass {

    private PosApi mPosApi;
    // Check on battery
    private int level_battery = 50;
    // check if printer can print
    private boolean isCanPrint = true;
    // Print queue assignment
    private PrintQueue mPrintQueue = null;
    private Context mContext;

    PrinterClass(Context mContext) {
        this.mContext = mContext;
        mPosApi = PosApi.getInstance(mContext);
    }

    PrintQueue getmPrintQueue() {
        return mPrintQueue;
    }

    // Get battery
    private void getBatteryNum() {
        BroadcastReceiver receiver = new BatteryBroadcastReceiver();
        mContext.registerReceiver(receiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        mContext.unregisterReceiver(receiver);
    }

    /**
     * Prompt box
     *
     * @param msg Prompt content
     */
    private void showTip(String msg) {
        new AlertDialog.Builder(mContext)
                .setTitle("Prompt")
                .setMessage(msg)
                .setNegativeButton("CLOSE",
                        (dialog, which) -> dialog.dismiss()).show();
    }

    void setUpPrinter() {
        mPrintQueue = new PrintQueue(mContext, mPosApi);
        // Print queue initialization
        mPrintQueue.init();
        // Print queue settings listener
        mPrintQueue.setOnPrintListener(new OnPrintListener() {
            // Print completed
            @Override
            public void onFinish() {
                // Print completed
                Toast.makeText(mContext,
                        "Print completed", Toast.LENGTH_SHORT)
                        .show();
                // Currently printable
                isCanPrint = true;
            }

            // Print failed
            @Override
            public void onFailed(int state) {
                isCanPrint = true;
                switch (state) {
                    case PosApi.ERR_POS_PRINT_NO_PAPER:
                        // Printer out of paper
                        showTip("Print result: printer is out of paper");
                        break;
                    case PosApi.ERR_POS_PRINT_FAILED:
                        // Print failed
                        showTip("Print result: Printer failed");
                        break;
                    case PosApi.ERR_POS_PRINT_VOLTAGE_LOW:
                        // Low voltage
                        showTip("Print result: battery voltage is too low");
                        break;
                    case PosApi.ERR_POS_PRINT_VOLTAGE_HIGH:
                        // High voltage
                        showTip("Print result: Power supply voltage is too high");
                        break;
                }
            }

            @Override
            public void onGetState(int arg0) {
            }

            // Printing settings
            @Override
            public void onPrinterSetting(int state) {
                isCanPrint = true;
                switch (state) {
                    case 0:
                        Toast.makeText(mContext, "Continuous paper", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        // Out of paper
                        Toast.makeText(mContext, "Out of paper", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        // Black mark detected
                        Toast.makeText(mContext, "Black mark detected", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    String printHere(String filePath) {
        try {
            String result = printImage(filePath);
            if (result.equals("Success")) {
                return printText("Powered by Emix Technologies" + "\n");
            } else return result;
        } catch (Exception ex) {
            return "ERROR! " + ex.getMessage();
        }
    }

    // call printer to print text
    String printText(String text) {
        if (!isCanPrint) return "Not printable";
        try {
            // check battery
            getBatteryNum();
            // Do not print when the battery is lower than 12%, and pop up a prompt
            if (level_battery <= 12) {
                Toast.makeText(mContext, "Low power,can't print!", Toast.LENGTH_SHORT).show();
                return "Low power,can't print!";
            }
            // else print text
            int concentration = 35;

            try {
                byte[] data = (text).getBytes("GBK");
                if (data != null) {
                    // 1 times font size
                    byte[] _1x = new byte[]{0x1b, 0x57, 0x01};
                    byte[] mData;
                    mData = new byte[3 + data.length];
                    // 1x font size default
                    System.arraycopy(_1x, 0, mData, 0, _1x.length);
                    System.arraycopy(data, 0, mData, _1x.length, data.length);

                    mPrintQueue.addText(concentration, mData);
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Set to not printable
            isCanPrint = false;
            // Print queue starts executing
            mPrintQueue.printStart();
            return "Success";
        } catch (Exception ex) {
            return "Oops! Something went wrong " + ex.getMessage();
        }
    }

    private String printImage(String filePath) {
        try {
            File imgFile = new File(filePath);
            // Bitmap to hold image
            Bitmap mBitmap;
            if (imgFile.exists()) {
                mBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            } else {
                mBitmap = null;
            }
            // Return when the image is empty
            if (mBitmap == null)
                return "No image found";
            // check battery
            getBatteryNum();
            // This operation is not performed during printing.
            if (!isCanPrint) return "Printing can not happen currently";
            // check battery
            getBatteryNum();
            // Do not print when the battery is lower than 12%, and pop up a prompt
            if (level_battery <= 12) {
                Toast.makeText(mContext, "Low power,can't print!", Toast.LENGTH_SHORT).show();
                return "Low power,can't print!";
            }
            // Get the left distance of the print
            int mLeft = 0;
            byte[] printData = BitmapTools.bitmap2PrinterBytes(mBitmap);
            int concentration = 35;

            mPrintQueue.addBmp(concentration, mLeft, mBitmap.getWidth(),
                    mBitmap.getHeight(), printData);

            try {
                mPrintQueue.addText(concentration, "\n\n\n\n\n"
                        .getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Print queue starts executing
            mPrintQueue.printStart();
            return "Success";
        } catch (Exception ex) {
            return "Oops! Something went wrong " + ex.getMessage();
        }
    }

    /*
     * Accept power change broadcast
     */
    class BatteryBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (Objects.equals(intent.getAction(), Intent.ACTION_BATTERY_CHANGED)) {

                level_battery = intent.getIntExtra("level", 0);

            }
        }
    }
}