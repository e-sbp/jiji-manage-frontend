package com.example.jiji_manage1.database;

// Here, we will use to organize all our database-related code

public class SbpDetailsDbSchema {

    // Define table and column tables
    public static final class SbpTable {
        public static final String NAME = "details";

        public static final class Cols {
            public static final String URL = "url";
            public static final String DEVICE_ID = "deviceID";
            public static final String API_KEY = "apiKey";
        }
    }
}
