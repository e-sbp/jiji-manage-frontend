package com.example.jiji_manage1.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Handles tasks like:
 * 1. Check to see if the database already exists.
 * 2. If it does not, create it and create the tables and initial data it needs.
 * 3. If it does, open it up and see what version of your CrimeDbSchema it has.
 * (You may want to add or remove things in future versions of CriminalIntent.)
 * 4. If it is an old version, run code to upgrade it to a newer version.
 */

public class SbpBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "sbpDetails.db";

    public SbpBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create TABLE " + SbpDetailsDbSchema.SbpTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                SbpDetailsDbSchema.SbpTable.Cols.URL + ", " +
                SbpDetailsDbSchema.SbpTable.Cols.DEVICE_ID + ", " +
                SbpDetailsDbSchema.SbpTable.Cols.API_KEY +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SbpDetailsDbSchema.SbpTable.NAME);
        onCreate(db);
    }
}
