package com.example.jiji_manage1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.jiji_manage1.database.SbpBaseHelper;
import com.example.jiji_manage1.database.SbpDetailsDbSchema.SbpTable;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.math.BigInteger;
import java.security.SecureRandom;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "flutter.native/helper";
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    private static String uniqueID = null;
    public BackgroundService gpsService;

    /* UUID.randomUUID() method generates an unique identifier for a specific installation.
       You have just to store that value and your user will be identified at the next launch of your
       application.
    */
    private synchronized static String generateDeviceID(Context context) {
        SecureRandom random = new SecureRandom();
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = new BigInteger(40, random).toString(10);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }

        return uniqueID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);

        ServiceConnection serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                String name = className.getClassName();
                if (name.endsWith("BackgroundService")) {
                    gpsService = ((BackgroundService.LocationServiceBinder) service).getService();
                }
            }

            public void onServiceDisconnected(ComponentName className) {
                if (className.getClassName().equals("BackgroundService")) {
                    gpsService = null;
                }
            }
        };

        checkLocationPermission();

        receiveFlutterCalls();

        final Intent intent = new Intent(this.getApplication(), BackgroundService.class);
        this.getApplication().startService(intent);
        this.getApplication().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void receiveFlutterCalls() {
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                (methodCall, result) -> {
                    switch (methodCall.method) {
                        case "generateDeviceID":
                            try {
                                result.success(generateDeviceID(getApplicationContext()));
                            } catch (Exception ex) {
                                result.error("ERROR!", "Something went wrong " + ex.getMessage(), null);
                            }
                            break;
                        case "saveSBPDetails":
                            try {
                                String device_id = methodCall.argument("device_id");
                                device_id = device_id != null ? device_id : "";
                                String base_url = methodCall.argument("base_url");
                                base_url = base_url != null ? base_url : "";
                                String api_key = methodCall.argument("api_key");
                                api_key = api_key != null ? api_key : "";
                                String response = saveSBPDetails(device_id, base_url, api_key);
                                gpsService.startTracking(true);
                                if (response.equals("Success")) result.success(response);
                                else result.error("ERROR!", response, null);
                            } catch (Exception ex) {
                                result.error("ERROR!", "Something went wrong " + ex.getMessage(), null);
                            }
                            break;
                        case "checkLocationPermission":
                            try {
                                result.success(checkLocationPermission());
                            } catch (Exception ex) {
                                result.error("ERROR!", "Something went wrong " + ex.getMessage(), null);
                            }
                            break;
                        case "stopTracking":
                            try {
                                gpsService.startTracking(false);
                                result.success("Tracking paused");
                            } catch (Exception ex) {
                                result.error("ERROR!", "Something went wrong " + ex.getMessage(), null);
                            }
                            break;
                        case "restartTracking":
                            try {
                                gpsService.startTracking(true);
                                result.success("Tracking restarted");
                            } catch (Exception ex) {
                                result.error("ERROR!", "Something went wrong " + ex.getMessage(), null);
                            }
                            break;
                        case "printText":
                            try {
                                String str = methodCall.argument("text");
                                str = str != null ? str : "";
                                PrinterClass printerClass = new PrinterClass(this);
                                printerClass.setUpPrinter();
                                String response = printerClass.printText(str);
                                result.success(response);
                                if (printerClass.getmPrintQueue() != null) {
                                    printerClass.getmPrintQueue().close();
                                }
                            } catch (Exception ex) {
                                result.error("PRINTING ERROR!", "Something went wrong" + ex.getMessage(), null);
                            }
                            break;
                        default:
                            result.notImplemented();
                            break;
                    }
                }
        );
    }

    private String checkLocationPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        if (gpsService != null) {
                            gpsService.startTracking(true);
                            Log.e("Running", "Tracking is on");
                        }
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) showSettingsDialog();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(error -> Toast.makeText(getApplicationContext(),
                        "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
        return "Okay";
    }

    private String saveSBPDetails(String device_id, String base_url, String api_key) {
        try {
            // create reference to db
            SQLiteDatabase mDatabase = new SbpBaseHelper(getApplicationContext()).getWritableDatabase();
            // key-value class to store data before saving to SQLite Database
            ContentValues values = new ContentValues();
            values.put(SbpTable.Cols.API_KEY, api_key);
            values.put(SbpTable.Cols.DEVICE_ID, device_id);
            String url = base_url.concat("700/device/gps/");
            values.put(SbpTable.Cols.URL, url);
            // save SBP details in db
            mDatabase.insert(SbpTable.NAME, null, values);
            return "Success";
        } catch (Exception ex) {
            return "Something went wrong: " + ex.getMessage();
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this location." +
                " You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.cancel();
            Toast.makeText(MainActivity.this, "Okay. Good bye", Toast.LENGTH_SHORT).show();
            MainActivity.this.finish();
            MainActivity.this.moveTaskToBack(true);
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
