package com.example.jiji_manage1;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.posapi.PosApi;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

/**
 * Scanning service class
 * Call the service class scanned by PDA
 *
 * @author wsl
 */
public class ScanService extends Service {

    public static PosApi mApi = null;
    // GPIO (General Purpose Input Output) power control
    static byte mGpioPower = 0x1E;// PB14
    // Serial port number and baud rate setting
    static int mCurSerialNo = 3; // usart3
    static int mBaudrate = 4; // 9600
    private static byte mGpioTrig = 0x29;// PC9
    boolean isScan = false;
    Handler handler = new Handler();
    Runnable run = () -> {
        // Pull down the scan head voltage after a certain period of time, turn off the scanning light
        ScanService.mApi.gpioControl(mGpioTrig, 0, 1);
        isScan = false;
    };
    // SCAN button monitor
    ScanBroadcastReceiver scanBroadcastReceiver;
    // F3 key monitor
    ScanBroadcastReceiver_F3 scanBroadcastReceiver_F3;
    // Audio control
    AudioManager audioManager = null;
    // Scanning tone player
    private MediaPlayer player;
    /**
     * Scanning head scanning information receiver
     */
    BroadcastReceiver receiver_ = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Objects.requireNonNull(action).equalsIgnoreCase(PosApi.ACTION_POS_COMM_STATUS)) {
                int cmdFlag = intent.getIntExtra(PosApi.KEY_CMD_FLAG, -1);
                byte[] buffer;
                buffer = intent.getByteArrayExtra(PosApi.KEY_CMD_DATA_BUFFER);
                // Serial port for transmitting scan information
                if (cmdFlag == PosApi.POS_EXPAND_SERIAL3) {
                    // If it is empty, return
                    if (buffer == null)
                        return;
                    // Play a scan tone, indicating that the message has been scanned
                    player.start();
                    try {
                        // Convert the byte bytes passed from the scan header into a string
                        String str = new String(buffer, "GBK");
                        Log.e("ScanStr", "-----:" + str.trim());
                        // Prepare to send scan information by broadcast,
                        // if it is integrated into your project, this paragraph can be ignored
                        Intent intentBroadcast = new Intent();
                        Intent intentBroadcast1 = new Intent();
                        // Set the action to send the broadcast
                        intentBroadcast.setAction("com.qs.scancode");
                        intentBroadcast1.setAction("com.zkc.scancode");
                        // Additional scan information to the intent
                        intentBroadcast.putExtra("code", str.trim());
                        intentBroadcast1.putExtra("code", str.trim());
                        // Send a broadcast, and register a receiver that receives the broadcast
                        // in Softkeyboard or Google Pinyin input method.
                        // After receiving the input method, the information will be input
                        // into the edit box currently in focus.
                        sendBroadcast(intentBroadcast);
                        sendBroadcast(intentBroadcast1);
                        // Set the current customer to scan again
                        isScan = false;
                        // Pull down the scan head voltage to extinguish the scan head
                        ScanService.mApi.gpioControl(mGpioTrig, 0, 1);
                        // Remove scan head to extinguish thread
                        handler.removeCallbacks(run);
                        // Shake according to the specified mode
                        // First argument: The first element in the array is waiting for how long it takes to start the vibrate.
                        // will be the duration of the vibration on and off, in milliseconds
                        // The second parameter: the index in the pattern when the vibration is repeated.
                        // If it is set to -1, it means no vibration.
//                            vibrator.vibrate(new long[]{1000,50,50,100,50},-1);
                        // Vibrate 2 seconds
//                            vibrator.vibrate(2000)

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    public static void init() {

        // mApi class assignment
        mApi = App.getInstance().getPosApi();
        // Delay the serial port for one second.
        // In order to initialize the scan head, it must be delayed by one second.
        // Otherwise, delay printing or no printing may occur
        // After delay, open the scan serial port
        new Handler().postDelayed(ScanService::openDevice, 1000);

    }

    // Open the scan head, note: pull down the voltage once, the scan head will light once
    public static void openScan() {
        // Pull down the scan head voltage
        ScanService.mApi.gpioControl(mGpioTrig, 0, 0);
        try {
            // Sleep 100ms (0.1 sec)
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Pull up the scan head voltage
        ScanService.mApi.gpioControl(mGpioTrig, 0, 1);
    }

    // Open serial port
    private static void openDevice() {
        // GPIO controller initialization
        mApi.gpioControl(mGpioPower, 0, 1);
        // Scanning serial port initialization
        mApi.extendSerialInit(mCurSerialNo, mBaudrate, 1, 1, 1, 1);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // initialization
        init();
        // Registered broadcast receiver
        registerListener();
        // Player instantiation
        player = MediaPlayer.create(getApplicationContext(), R.raw.beep);
        // Audio initialization
        audioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);

        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    // Convert a string to a GBK format string
    public String toGBK(String str) throws UnsupportedEncodingException {
        return this.changeCharset(str, "GBK");
    }

    /**
     * String encoding conversion implementation method
     *
     * @param str        The encoded string to be converted
     * @param newCharset Target code
     * @return Converted string
     * @throws UnsupportedEncodingException This is a possible error to be thrown
     */
    public String changeCharset(String str, String newCharset)
            throws UnsupportedEncodingException {
        if (str != null) {
            // Decode the string with the default character encoding.
            byte[] bs = str.getBytes(newCharset);
            // Generate a string with a new character encoding
            return new String(bs, newCharset);
        }
        return null;
    }

    @Override
    public void onDestroy() {
        // Close the entire lower serial port
        mApi.closeDev();
        super.onDestroy();
    }

    /**
     * Register broadcast receiver
     */
    private void registerListener() {

        // Register the receiver for scanning information
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(PosApi.ACTION_POS_COMM_STATUS);
        registerReceiver(receiver_, mFilter);
        // Receiver broadcasted when the SCAN button is pressed
        scanBroadcastReceiver = new ScanBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ismart.intent.scandown");
        this.registerReceiver(scanBroadcastReceiver, intentFilter);
        // The receiver that is broadcast when the F3 button is pressed
        // (the F3 button is only on the right side of the 5501 machine,
        // that is, the small button above the scan button)
        scanBroadcastReceiver_F3 = new ScanBroadcastReceiver_F3();
        IntentFilter intentFilter_f3 = new IntentFilter();
        intentFilter_f3.addAction("ismart.intent.f3down");
        this.registerReceiver(scanBroadcastReceiver_F3, intentFilter_f3);

    }

    // SCAN button monitoring
    class ScanBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isScan) {
                // Scan head is not in scan state
                // Open scan head
                ScanService.mApi.gpioControl(mGpioTrig, 0, 0);
                isScan = true;
                handler.removeCallbacks(run);
                handler.postDelayed(run, 3000);
            } else {
                // The scan head is in the scan head state, first turn off the scan head light
                ScanService.mApi.gpioControl(mGpioTrig, 0, 1);
                // Open scan head
                ScanService.mApi.gpioControl(mGpioTrig, 0, 0);
                isScan = true;
                handler.removeCallbacks(run);
                handler.postDelayed(run, 3000);
            }
        }
    }

    // F3 button monitoring
    class ScanBroadcastReceiver_F3 extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Outgoing volume adjustment box
//			audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
//					AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND
//							| AudioManager.FLAG_SHOW_UI);
            Toast.makeText(context, "F3 click", Toast.LENGTH_SHORT).show();
        }
    }


}
