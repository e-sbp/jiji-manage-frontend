package com.example.jiji_manage1;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import android.annotation.SuppressLint;
import android.text.TextUtils;

/**
 * Helper class to carry out android volley requests
 * This is a singleton with a private constructor and a public get method to access it
 */

class VolleyHelperClass {

    @SuppressLint("StaticFieldLeak")
    private static VolleyHelperClass sVolleyHelperClass;
    private static RequestQueue sRequestQueue;
    private static final String TAG = "Volley Helper Tag";
    private Context mContext;

    private VolleyHelperClass(Context context) {
        mContext = context;
        sRequestQueue = getRequestQueue();
    }

    /**
     * Provide access to instance of class
     *
     * @return sVolleyHelperClass - an instance of the class
     */
    static synchronized VolleyHelperClass getVolleyHelperClass(Context context) {
        if (sVolleyHelperClass == null) {
            sVolleyHelperClass = new VolleyHelperClass(context);
        }
        return sVolleyHelperClass;
    }

    /**
     * Helps to return the instance of RequestQueue provided by Volley
     *
     * @return sRequestQueue - an instance of RequestQueue
     */
    private RequestQueue getRequestQueue() {
        // instantiate only if null
        if (sRequestQueue == null) {
            sRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }

        return sRequestQueue;
    }

    /**
     * Method used to add new RequestQueues coming from different parts of the app
     *
     * @param request Request to be added
     * @param tag     Tag of Request. Helps us if we need to cancel a request later
     */
    @SuppressWarnings("SameParameterValue")
    private <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? /* if tag is empty set default tag */ TAG : tag);
        // set request call and add the request to the request queue
        getRequestQueue().add(request);
    }

    /**
     * Method to create request with a default tag
     */
    <T> void addToRequestQueue(Request<T> request) {
        addToRequestQueue(request, TAG);
    }

    /**
     * Method used to cancel all requests that have the passed tag
     *
     * @param tag Tag of Request
     */
    @SuppressWarnings("SameParameterValue")
    private void cancelRequestQueue(String tag) {
        if (sRequestQueue != null) {
            sRequestQueue.cancelAll(TextUtils.isEmpty(tag) ? TAG : tag);
        }
    }

    /**
     * Method to cancel request with default tag
     */
    void cancelRequestQueue() {
        cancelRequestQueue(TAG);
    }

}
