package com.example.jiji_manage1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.posapi.PosApi;
import android.util.Log;

import androidx.annotation.CallSuper;

import io.flutter.view.FlutterMain;

/**
 * Application class, please note that the mPosApi variable here can only be used in the entire code.
 * PosApi cannot be instantiated again, otherwise printing delay or no printing will occur.
 * Scan will scan twice or no scanning light, please note
 *
 * @author wsl
 */
@SuppressLint("Registered")
public class App extends Application {

    static App instance = null;
    //    PosSDK mSDK = null;
    static PosApi mPosApi = null;
    private static String mCurDev1 = "";
    private Activity mCurrentActivity = null;

    public App() {
        super.onCreate();
        instance = this;
    }

    public static void init() {
        // Initialize the mPosApi class according to the model
        if (Build.MODEL.contains("LTE") || Build.DISPLAY.contains("3508") ||
                Build.DISPLAY.contains("403") ||
                Build.DISPLAY.contains("35S09")) {
            mPosApi.initPosDev("ima35s09");
            setCurDevice("ima35s09");
        } else if (Build.MODEL.contains("5501")) {
            mPosApi.initPosDev("ima35s12");
            setCurDevice("ima35s12");
        } else {
            mPosApi.initPosDev(PosApi.PRODUCT_MODEL_IMA80M01);
            setCurDevice(PosApi.PRODUCT_MODEL_IMA80M01);
        }

    }

    public static App getInstance() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    @CallSuper
    public void onCreate() {
        super.onCreate();
        FlutterMain.startInitialization(this);
//        mDb = Database.getInstance(this);
        if (Build.MODEL.contains("PDA")) {
            Log.v("hello", "APP onCreate~~");
            // PosApi class initialization, this class is the core class of the project,
            // please be sure to instantiate, otherwise there will be phenomena such as unprintable & scanable.
            mPosApi = PosApi.getInstance(this);
            // initialization
            init();
        }
    }

    public Activity getCurrentActivity() {
        return this.mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    public String getCurDevice() {
        return mCurDev1;
    }

    public static void setCurDevice(String mCurDev) {
        mCurDev1 = mCurDev;
    }

    // Reference mPosApi variable elsewhere
    public PosApi getPosApi() {
        return mPosApi;
    }

}
