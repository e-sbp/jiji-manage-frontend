package com.example.jiji_manage1;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkingClass {
    private static final String TAG = "TAG";
    private static final String ERROR = "ERROR";
    private static JSONObject sJSONObject = new JSONObject();

    /**
     * Method that carries out POST requests using Android Volley library
     * This methods assumes that a JSON Object in a JSON Object response is returned upon request
     *
     * @param url     URL to request values from
     * @param context Current context of app
     */

    static JSONObject POST(String url, Context context, JSONObject json,
                           String api_key, String device_id) {
        try {
            JsonObjectRequest stringRequest = new JsonObjectRequest(
                    Request.Method.POST, url, json, response -> {
                try {
                    Log.e("INTERNET " + TAG, "" + response);
                    // parse fetched Json String to JSON Object
                    sJSONObject = response;
                } catch (Exception ex) {
                    Log.e("INTERNET " + TAG, "Something went wrong");
                    ex.printStackTrace();
                }
            }, error -> {
                Log.e(ERROR, "Something went wrong");
                Toast.makeText(context, "Internet or server down", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                sJSONObject = null;
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("api_key", api_key);
                    headers.put("device_id", device_id);
                    return headers;
                }
            };
            VolleyHelperClass.getVolleyHelperClass(context).addToRequestQueue(stringRequest);
        } catch (Exception ex) {
            Log.e(ERROR, "Something went wrong");
            ex.printStackTrace();
            sJSONObject = null;
        }

        return sJSONObject;
    }

    /**
     * Method that carries out GET requests using Android Volley library
     * This methods assumes that a JSON Object in a string response is returned upon request
     *
     * @param url     URL to request values from
     * @param context Current context of app
     */

    public static JSONObject GET(String url, Context context) {
        try {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {
                        try {
                            Log.e("INTERNET " + TAG, response);
                            // parse fetched Json String to JSON Object
                            sJSONObject = new JSONObject(response);
                        } catch (JSONException ex) {
                            Log.e("INTERNET " + TAG, "Something went wrong");
                            ex.printStackTrace();
                        }
                    }, error -> {
                Log.e(ERROR, "Something went wrong");
                error.printStackTrace();
                sJSONObject = null;
            });

            VolleyHelperClass.getVolleyHelperClass(context).addToRequestQueue(stringRequest);
        } catch (Exception ex) {
            Log.e(ERROR, "Something went wrong");
            ex.printStackTrace();
            sJSONObject = null;
        }

        return sJSONObject;
    }

    public static void cancelRequest(Context context) {
        try {
            VolleyHelperClass.getVolleyHelperClass(context).cancelRequestQueue();
        } catch (Exception ex) {
            Log.e(ERROR, "Something went wrong");
            ex.printStackTrace();
        }
    }

}
