package com.example.jiji_manage1.database;

import android.database.Cursor;
import android.database.CursorWrapper;

/**
 * A CursorWrapper lets you
 * wrap a Cursor you received from another place and add new methods on top of it
 */

public class SbpCursorWrapper extends CursorWrapper {
    public SbpCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public SBPDetails getDetails() {
        SBPDetails details = new SBPDetails();
        details.setApiKey(getString(getColumnIndex(SbpDetailsDbSchema.SbpTable.Cols.API_KEY)));
        details.setDeviceID(getString(getColumnIndex(SbpDetailsDbSchema.SbpTable.Cols.DEVICE_ID)));
        details.setUrl(getString(getColumnIndex(SbpDetailsDbSchema.SbpTable.Cols.URL)));

        return details;
    }
}
