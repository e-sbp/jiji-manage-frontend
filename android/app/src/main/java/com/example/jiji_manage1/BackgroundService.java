package com.example.jiji_manage1;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.example.jiji_manage1.database.SBPDetails;
import com.example.jiji_manage1.database.SbpBaseHelper;
import com.example.jiji_manage1.database.SbpCursorWrapper;
import com.example.jiji_manage1.database.SbpDetailsDbSchema.SbpTable;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides the background service of the app
 * Location tracking shall be done here
 * and the GPS coordinates obtained from device and sent to service from here.
 * To enable location tracking even when app is running in background,
 * we shall generate a permanent notification to serve as foreground service
 * when app is in background and also to indicate to user that the app is running.
 * Biggest disadvantage will draining battery when the app is picking coordinates,
 * hence the picking shall be well spaced out, eg after 10 mins.
 * Also, it is hoped that the PDA shall be used solely for scanning purpose, hence it's minimal use.
 */

public class BackgroundService extends Service {
    private final LocationServiceBinder binder = new LocationServiceBinder();
    private final String TAG = "BackgroundService";
    private LocationManager mLocationManager;
    private JSONObject jsonObject;

    /**
     * Method checks whether there is internet connectivity
     * It calls the getActiveNetworkInfo and then checks if returns null, which it then returns
     * as true for network connection or false if otherwise
     *
     * @param context Current context
     * @return State of net connection
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * This method creates a toast asking user to enable internet access
     */
    public static void requestInternetAccess(@NonNull final Context context) {
        if (!isNetworkAvailable(context)) {
            Toast.makeText(context, "Please enable internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate");
        startForeground(12345678, getNotification());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initializeLocationManager() {
        if (mLocationManager == null) {
            mLocationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
        }
    }

    /**
     * Create cursor to read data from table(s) in db
     * Database cursors are called cursors because they always have their finger on a particular place in a
     * query
     * Wrap in cursor wrapper to remove need for repeatedly querying table and to add new methos
     *
     * @param whereClause Specific condition to be met before querying
     * @param whereArgs   Specifying values for the arguments in the where clause
     * @return cursorWrapper
     */
    @SuppressWarnings("SameParameterValue")
    private SbpCursorWrapper querySBP(@Nullable String whereClause, @Nullable String[] whereArgs) {
        // create reference to db
        SQLiteDatabase mDatabase = new SbpBaseHelper(getApplicationContext()).getWritableDatabase();
        // Create string array for columns containing SBP Details
        String[] details = {
                SbpTable.Cols.API_KEY,
                SbpTable.Cols.DEVICE_ID,
                SbpTable.Cols.URL,
        };
        Cursor cursor = mDatabase.query(
                SbpTable.NAME,
                details, // null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new SbpCursorWrapper(cursor);
    }

    public List<SBPDetails> getDetails() {
        List<SBPDetails> details = new ArrayList<>();
        try (SbpCursorWrapper cursor = querySBP(null, null)) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                details.add(cursor.getDetails());
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            Log.e("DATABASE ERROR", "Something went wrong " + ex.getMessage());
        }
        return details;
    }

    public void startTracking(boolean isTrack) {
        try (SbpCursorWrapper cursorWrapper = querySBP(null, null)) {
            // if no saved details, exit
            if (cursorWrapper.getCount() == 0) return;

            if (getDetails().isEmpty() || getDetails().size() == 0) return;

            // query db for SBP details
            cursorWrapper.moveToFirst();
            SBPDetails sbpDetails = cursorWrapper.getDetails();

            String url = sbpDetails.getUrl(),
                    api_key = sbpDetails.getApiKey(),
                    device_id = sbpDetails.getDeviceID();

            // set up location checking
            initializeLocationManager();

            // Check whether GPS tracking is enabled
            if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return;
            }

            // Check whether this app has access to the location permission

            int permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            // If the location permission has been granted, then start the TrackerService

            if (permission == PackageManager.PERMISSION_GRANTED) {

                try {
                    int LOCATION_INTERVAL = 60 * 1000; // 10 minutes in milliseconds
                    float LOCATION_DISTANCE = 0.0f;
                    LocationRequest request = new LocationRequest();
                    // Specify how often your app should request the device’s location
                    request.setInterval(LOCATION_INTERVAL);
                    // Specify how distance change between locations
                    request.setSmallestDisplacement(LOCATION_DISTANCE);
                    // Get the most accurate location data available
                    request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    // Set up location provider client
                    FusedLocationProviderClient client = LocationServices
                            .getFusedLocationProviderClient(this);
                    // Set up callback for location change
                    LocationCallback mLocationCallback = new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            Location location = locationResult.getLastLocation();
                            if (location != null) {
                                try {
                                    if (isNetworkAvailable(BackgroundService.this)) {
                                        URLEncoder.encode(url, "UTF8");

                                        // Creating the JSON to post to the API
                                        JSONObject json = new JSONObject();
                                        json.put("longitude", String.valueOf(location.getLongitude()));
                                        json.put("latitude", String.valueOf(location.getLatitude()));

                                        // post GPS coordinates to server
                                        jsonObject = NetworkingClass.POST(url,
                                                BackgroundService.this, json, api_key, device_id);
                                        Log.e(TAG, jsonObject.toString());
                                    } else requestInternetAccess(BackgroundService.this);
                                } catch (UnsupportedEncodingException e) {
                                    Toast.makeText(BackgroundService.this,
                                            "No result found", Toast.LENGTH_SHORT).show();
                                    Log.e(TAG, "fail to encode string: ", e);
                                } catch (JSONException err) {
                                    Toast.makeText(BackgroundService.this,
                                            "No result found", Toast.LENGTH_SHORT).show();
                                    Log.i(TAG, "Error dealing with json: ", err);
                                } catch (Exception ex) {
                                    Log.e(TAG, "Something went wrong: ", ex);
                                }
                            }
                        }
                    };

                    // check if tracking enabled
                    if (isTrack) {
                        // then request location updates
                        client.requestLocationUpdates(request, mLocationCallback, null);
                    } else {
                        // or remove location updates
                        client.removeLocationUpdates(mLocationCallback);
                    }

                } catch (SecurityException ex) {
                    Log.i(TAG, "fail to request location update, ignore", ex);
                } catch (IllegalArgumentException ex) {
                    Log.d(TAG, "gps provider does not exist " + ex.getMessage());
                } catch (Exception ex) {
                    Log.e(TAG, "something just went wrong " + ex.getMessage());
                }
            } else {

                // If the app does not currently have access to the user’s location, then request access

                ActivityCompat.requestPermissions((Activity) getApplicationContext(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        28401);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "No result found", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Error dealing with json: ", ex);
        }
    }

    private Notification getNotification() {

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String NOTIFICATION_CHANNEL_ID = "eSBP_01";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel =
                    new NotificationChannel(NOTIFICATION_CHANNEL_ID, "e-SBP Notifications",
                            NotificationManager.IMPORTANCE_DEFAULT);

            // Configure the notification channel.
            notificationChannel.setDescription("Sample Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        // Redirect to main activity when user taps on notification
        Intent intent = new Intent(BackgroundService.this, MainActivity.class);

        PendingIntent contentIntent =
                PendingIntent.getActivity(this, 0, intent, 0);

        // Create and set features of notification
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        notificationBuilder.setAutoCancel(true)
                .setContentIntent(contentIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_notif_img)
                .setTicker("eSBP")
                .setContentTitle("e-SBP")
                .setContentText("e-SBP is running")
                .setOngoing(true)
                .setContentInfo("Information");

        return notificationBuilder.build();
    }

    class LocationServiceBinder extends Binder {
        BackgroundService getService() {
            return BackgroundService.this;
        }
    }

}
