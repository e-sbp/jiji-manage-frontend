/// Set up base for CRUD

import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'models.dart';

class DBProvider {
  // create singleton to ensure there is only one class instance with a global access point
  DBProvider._();

  static final DBProvider db = DBProvider._();

  // Here is the database object with a getter
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // instantiate database if not null
    _database = await createDatabase("e-SBP.db");
    return _database;
  }

  Future<Database> createDatabase(String dbName) async {
    String databasesPath = await getDatabasesPath();
    String dbPath = join(databasesPath, dbName);

    return await openDatabase(dbPath,
        version: 1, onCreate: populateDb, onOpen: (db) {});
  }

  /// When creating the database, create the table

  void populateDb(Database database, int version) async {
    await database.execute('CREATE TABLE User ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT, '
        'user_id INTEGER NOT NULL UNIQUE, '
        'username TEXT NOT NULL UNIQUE, '
        'fullname TEXT NOT NULL, '
        'user_role TEXT NOT NULL, '
        'token TEXT NOT NULL'
        ')');
    await database.execute('CREATE TABLE SBP_CMI ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT, '
        'device_id TEXT NOT NULL UNIQUE, '
        'base_url_cmi TEXT NOT NULL UNIQUE, '
        'base_url_sbp TEXT NOT NULL UNIQUE'
        ')');
  }

  Future<int> createUser(User user) async {
    final db = await database;
    var result = await db.insert("User", user.toJson());
    return result;
  }

  Future<int> createCMIDetails(CMI cmi) async {
    final db = await database;
    var result = await db.insert("SBP_CMI", cmi.toJson());
    return result;
  }

  Future<List> getAllUsers() async {
    final db = await database;
    var result = await db.query("User", columns: ["username", "user_role"]);
    return result.toList();
  }

  Future<List> getAllCMIDetails() async {
    final db = await database;
    var result = await db.query(
      "SBP_CMI",
      columns: ["device_id", "base_url_cmi", "base_url_sbp"],
    );
    return result.toList();
  }

  Future<User> getUser(String username) async {
    final db = await database;
    List<Map> results =
        await db.query("User", where: "username = ?", whereArgs: [username]);
    return results.isNotEmpty ? User.fromJson(results.first) : null;
  }

  Future<CMI> getDetail(int id) async {
    final db = await database;
    List<Map> results =
        await db.query("SBP_CMI", where: "id = ?", whereArgs: [id]);
    return results.isNotEmpty ? CMI.fromJson(results.first) : null;
  }

  Future<int> updateUser(User user) async {
    final db = await database;
    return await db.update("User", user.toJson(),
        where: "username = ?", whereArgs: [user.username]);
  }

  Future<int> updateDetail(CMI cmi) async {
    final db = await database;
    return await db.update("SBP_CMI", cmi.toJson(),
        where: "device_id = ?", whereArgs: [cmi.deviceID]);
  }

  Future<int> deleteUser(String username) async {
    final db = await database;
    return await db
        .delete("User", where: "username = ?", whereArgs: [username]);
  }

  Future<int> deleteDetails(int id) async {
    final db = await database;
    return await db.delete("SBP_CMI", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from SBP_CMI");
    db.rawDelete("Delete * from User");
  }
}
