/// Define models for the SQLite database

/// Create model for User

class User {
  int userID;
  String username;
  String fullName;
  String token;
  String role;

  User({
    this.userID,
    this.username,
    this.fullName,
    this.role,
    this.token,
  });

  factory User.fromJson(Map<String, dynamic> data) {
    return User(
      userID: data["user_id"] ?? '',
      username: data["username"] ?? '',
      fullName: data["name"] ?? '',
      role: data["user_role"] ?? '',
      token: data["token"] ?? '',
    );
  }

  Map<String, dynamic> toJson() => {
        "user_id": userID,
        "fullname": fullName,
        "username": username,
        "user_role": role,
        "token": token,
      };
}

class CMI {
  String deviceID;
  String cmiURL;
  String sbpURL;

  CMI({
    this.deviceID,
    this.cmiURL,
    this.sbpURL,
  });

  factory CMI.fromJson(Map<String, dynamic> data) {
    return CMI(
      deviceID: data["device_id"] ?? '',
      cmiURL: data["base_url_cmi"] ?? '',
      sbpURL: data["base_url_sbp"] ?? '',
    );
  }

  Map<String, dynamic> toJson() => {
        "base_url_sbp": sbpURL,
        "base_url_cmi": cmiURL,
        "device_id": deviceID,
      };
}
