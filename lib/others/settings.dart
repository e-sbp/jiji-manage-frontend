/// Here we shall set up granting permissions and opening settings

import 'package:jiji_manage1/others/extras.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:access_settings_menu/access_settings_menu.dart';

// Check location permission
Future<bool> checkPermission(PermissionGroup permission) async {
  PermissionStatus status =
      await PermissionHandler().checkPermissionStatus(permission);
  if (status == PermissionStatus.granted)
    return true;
  else
    return false;
}

// Request location permission
Future requestPermission(PermissionGroup permission) async {
  Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler().requestPermissions([permission]);
  print('Permission request result: $permissions');
}

// Check on location permission status after requesting
Future<bool> getPermissionStatus(PermissionGroup permission) async {
  return checkPermission(permission);
}

// Show this when user encounters any error
Widget errorResult(String err) {
  return Column(children: <Widget>[
    Padding(padding: EdgeInsets.only(top: 0.0)),
    Container(
      child: Icon(Icons.error, color: Colors.black, size: 100.0),
      width: 200,
      height: 200,
    ),
    Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
    Text(err, style: TextStyle(color: Colors.black, fontSize: 18.0)),
    Padding(padding: EdgeInsets.only(bottom: 50.0)),
  ]);
}

// create an async void to call the API function with settings name as parameter
Future<bool> openSettingsMenu(settingsName) async {
  var resultSettingsOpening = false;

  try {
    resultSettingsOpening =
        await AccessSettingsMenu.openSettings(settingsType: settingsName);
  } catch (e) {
    logError("ERROR", e.toString());
    resultSettingsOpening = false;
  }

  return resultSettingsOpening;
}

Future<ServiceStatus> checkServiceStatus(PermissionGroup permission) {
  return PermissionHandler().checkServiceStatus(permission);
}

Future<LocationData> getLocation() async {
  Location _locationService = new Location();
  bool _permission = false;
  String error;
  LocationData location;

  await _locationService.changeSettings(accuracy: LocationAccuracy.HIGH);

  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    bool serviceStatus = await _locationService.serviceEnabled();
    print("Service status: $serviceStatus");
    if (serviceStatus) {
      _permission = await _locationService.requestPermission();
      print("Permission: $_permission");
      if (_permission) location = await _locationService.getLocation();
    } else {
      bool serviceStatusResult = await _locationService.requestService();
      print("Service status activated after request: $serviceStatusResult");
      if (serviceStatusResult) getLocation();
    }
  } on PlatformException catch (e) {
    if (e.code == 'PERMISSION_DENIED') {
      logError(e.code, e.message);
      error = e.message;
    } else if (e.code == 'SERVICE_STATUS_ERROR') {
      logError(e.code, e.message);
      error = e.message;
    }
    showToast(error);
    location = null;
  }

  return location;
}
