/// define all colors here
import 'package:flutter/material.dart';

const myAccentColor = const Color(0xFF03A9F4);
const myPrimaryColor = const Color(0xFF2196F3);
const myDarkPrimaryColor = const Color(0xFF1976D2);
const myLightPrimaryColor = const Color(0xFFBBDEFB);
const myBackgroundColor = const Color(0xFFFFFFFF);
const myPrimaryTextColor = const Color(0xFF212121);
const mySecondaryTextColor = const Color(0xFF757575);
const myErrorColor = const Color(0xFFE53935);
const myDividerColor = const Color(0xFFBDBDBD);
const myLoginColor = const Color(0xFF00FF00);
