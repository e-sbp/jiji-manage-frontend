import 'package:fluttertoast/fluttertoast.dart';
import 'package:device_info/device_info.dart';
import 'dart:io' show Platform;
import 'package:flutter/services.dart';

/// this is a function to show a toast

showToast(String message, {Toast duration = Toast.LENGTH_SHORT}) =>
    Fluttertoast.showToast(
        msg: message, toastLength: duration, timeInSecForIos: 1);

/// This will print errors to console

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

/// Confirm if string is empty

bool isStringEmpty(String str) =>
    str == null || str == '' || str.length == 0 ? true : false; //

/// Check that string entered is a valid email address

bool validateEmail(String email) {
  RegExp regExp = new RegExp(
    r"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}",
    caseSensitive: false,
    multiLine: false,
  );
  return regExp.hasMatch(email);
}

/// Check device model number
/// PDA model number is PDA3505

Future<String> deviceModel() async {
  try {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      return androidInfo.model;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      return iosInfo.utsname.machine;
    } else {
      print("Device info missing");
      return Future.value("");
    }
  } catch (err) {
    print("Device info error: $err");
    return Future.value("");
  }
}

/// Use this method to call printer

Future<void> printReceipt(String text) async {
  String devModel = await deviceModel();
  if (devModel != "PDA3505") {
    showToast("Successful verification");
    return;
  }
  String response = '';
  try {
    final String result =
        await MethodChannel('flutter.native/helper').invokeMethod(
      "printText",
      {
        "text": text,
      },
    );
    response = result;
  } on PlatformException catch (e) {
    response = "Failed to Invoke: '${e.message}'.";
  }

  if (response == "Success")
    showToast("Printing ongoing");
  else
    showToast(response);

  print(response);
}
