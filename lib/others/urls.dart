/// Define app urls here

import 'dart:async';
import '../database/models.dart';
import '../database/database.dart';

final stator =
    "http://197.248.6.132:8134/CentralMobileInterface/emix/mobile/event";

Future<CMI> get detail async {
  DBProvider db = DBProvider.db;
  return await db.getDetail(1);
}

Future<String> get apiKey async {
  return "0d1d07e612f892e0211d9738b56568d0c5a4b1723af18305366908d8bc58ef75";
}

Future<String> get deviceId async {
  CMI cmi = await detail;
  return cmi.deviceID;
}

Future<String> get sbp async {
  CMI cmi = await detail;
  return cmi.sbpURL;
}

Future<String> get cmi async {
  CMI cmi = await detail;
  return cmi.cmiURL;
}

String start(int userId, int mssdn) {
  return "$stator/1/user/$userId/$mssdn/";
}

Future<String> login() async {
  String c = await cmi;
  return "${c}100/user/login/";
}

Future<String> logout() async {
  String c = await cmi;
  return "${c}100/user/logout/";
}

Future<String> verify(
    String code, String id, String latitude, String longitude) async {
  String s = await sbp;
  return "${s}100/verify?qrcode=$code&business_id=$id&latitude=$latitude&longitude=$longitude";
}

Future<String> registerQR() async {
  String s = await sbp;
  return "${s}200/register/qrcode";
}

Future<String> flagQRCode() async {
  String s = await sbp;
  return '${s}300/flag/qrcode';
}

Future<String> getAllBusinesses() async {
  String s = await sbp;
  return '${s}400/qrcode/all';
}
