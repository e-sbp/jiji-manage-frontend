/// setting of paths to different screens happens here

import 'package:flutter/material.dart';
import 'package:jiji_manage1/screens/login.dart';
import 'package:jiji_manage1/screens/splash.dart';
import 'package:jiji_manage1/screens/welcome.dart';

// create paths
final routes = <String, WidgetBuilder>{
  '/': (_) => SplashScreen(),
  '/login': (_) => LoginScreen(),
  '/welcome': (_) => WelcomeScreen(),
};
