import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';

import 'package:jiji_manage1/others/themes.dart';
import 'package:jiji_manage1/others/routes.dart';

//void main() {
//  // use system chrome to control orientation(set to portrait)
//  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
//      .then((_) {
//    runApp(MyApp());
//  });
//}
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // hide debug banner during development
      debugShowCheckedModeBanner: false,
      title: 'Jiji QR Scan',
      color: Colors.blue,
      theme: myAppTheme,
      routes: routes,
    );
  }
}
