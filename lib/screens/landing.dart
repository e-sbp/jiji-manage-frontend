/// This is the landing page
/// It will hold tabs for home, QR scan and register QR
/// Hope it makes sense

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../others/urls.dart';
import '../components/title.dart';
import '../database/models.dart';
import '../database/database.dart';
import 'home.dart' as home;
import 'scan_qr.dart' as scan;
import 'locate.dart' as locate;
import 'register_qr.dart' as register;
import '../components/fancy_fab.dart';
import '../others/extras.dart';

class LandingPage extends StatefulWidget {
  final String username;

  LandingPage(this.username);

  @override
  _LandingPageState createState() => _LandingPageState();
}

Future<dynamic> handleSignOut(String username) async {
  final User user = await DBProvider.db.getUser(username);
  final response = await http.get(
    await logout(),
    headers: {
      'token': user.token,
      'user_name': user.username,
      'api_key': await apiKey,
    },
  );
  if (response.statusCode == 200) {
    Map resp = json.decode(response.body);
    Map res = resp['Response'][0];
    return res;
  } else {
    logError("${response.statusCode}", "${response.body}");
    return "Something went wrong. Please try again later";
  }
}

void logOut(BuildContext context, String username) async {
  final User user = await DBProvider.db.getUser(username);
  showToast("Please wait...");
  try {
    handleSignOut(username).then((res) {
      final errorCode = res['errorCode'];
      if (errorCode == null) {
        showToast("Error while logging out");
        return;
      }
      if (int.tryParse(errorCode) == 200) {
        user.token = '';
        DBProvider.db.updateUser(user).then((value) {
          showToast('Successful logout');
          Navigator.pushReplacementNamed(context, "/login");
        }).catchError((e) {
          logError("LOGOUT ERROR", "Error while updating token: $e");
          showToast("Unable to logout. Please try again later");
        });
      } else {
        logError("LOGOUT ERROR", "Error: $errorCode");
        showToast("Failed log out");
      }
    });
  } catch (err) {
    showToast("Something went wrong. Please try again later");
    print("$err");
  }
}

// ticker provider enables navigation with animation
class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  // Manages the state required by TabBar and a TabBarView.
  TabController controller;

  // list all of the landing page tabs
  final List<Tab> landingTabs = <Tab>[
    Tab(
      child: Column(
        children: <Widget>[
          Icon(FontAwesomeIcons.home, color: Colors.white),
          Padding(padding: EdgeInsets.only(top: 1.0)),
          Text('Home', style: TextStyle(color: Colors.white)),
          Padding(padding: EdgeInsets.only(bottom: 1.0)),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Icon(FontAwesomeIcons.qrcode, color: Colors.white),
          Padding(padding: EdgeInsets.only(top: 1.0)),
          Text('Scan', style: TextStyle(color: Colors.white)),
          Padding(padding: EdgeInsets.only(bottom: 1.0)),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Icon(FontAwesomeIcons.plus, color: Colors.white),
          Padding(padding: EdgeInsets.only(top: 1.0)),
          Text('Register',
              style: TextStyle(color: Colors.white, fontSize: 13.4)),
          Padding(padding: EdgeInsets.only(bottom: 1.0)),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Icon(FontAwesomeIcons.map, color: Colors.white),
          Padding(padding: EdgeInsets.only(top: 1.0)),
          Text('Locate', style: TextStyle(color: Colors.white)),
          Padding(padding: EdgeInsets.only(bottom: 1.0)),
        ],
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    controller = TabController(
      vsync: this, // create object that manages state required for tabs
      length: landingTabs.length, // length is the number of tabs
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<bool> _exitHome(BuildContext context) {
    showToast("Please logout first");
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: AppTitle(),
          // bottom allows for us to place tabs at the top of the page
          bottom: TabBar(
            tabs: landingTabs,
            controller: controller,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.exit_to_app,
                color: Colors.white,
                size: 22.0,
                semanticLabel: "Logout",
              ),
              tooltip: "Logout",
              onPressed: () => logOut(context, widget.username),
            ),
            Padding(padding: EdgeInsets.only(right: 10.0)),
          ],
        ),
        floatingActionButton: FancyFab(tabController: controller),
        body: TabBarView(
          children: <Widget>[
            home.HomeScreen(),
            scan.QRScanScreen(widget.username),
            register.QRRegisterScreen(widget.username),
            locate.LocationsScreen(widget.username),
          ],
          controller: controller,
        ),
      ),
      onWillPop: () => _exitHome(context),
    );
  }
}
