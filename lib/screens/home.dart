/// This is the home page

import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Icon(
          Icons.home,
          size: 150.0,
        ),
      ),
    );
  }
}
