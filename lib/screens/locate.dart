/// Here we shall map out all businesses
/// valid or not

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'landing.dart';
import '../others/urls.dart';
import '../others/extras.dart';
import '../others/settings.dart';
import '../database/models.dart';
import '../database/database.dart';
import '../components/title.dart';
import '../components/scan_button.dart';

class LocationsScreen extends StatefulWidget {
  final String username;

  LocationsScreen(this.username);

  static restartApp(BuildContext context, String errorCode) {
    print("Context: $context");
    _LocationsScreenState state =
        context.ancestorStateOfType(const TypeMatcher<_LocationsScreenState>());
    print("State: $state");
    state.restartApp(errorCode);
  }

  @override
  _LocationsScreenState createState() => _LocationsScreenState();
}

class _LocationsScreenState extends State<LocationsScreen> {
  Key _key = UniqueKey();
  String _code = "200";

  void restartApp(String errCode) {
    this.setState(() {
      _key = UniqueKey();
      _code = errCode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      key: _key,
      child: ScanButton(
        onPressed: () => Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (_) {
              return LocationsPage(widget.username, code: _code);
            },
          ),
        ),
        text: 'Locate Businesses',
        icon: FontAwesomeIcons.mapMarkerAlt,
      ),
    );
  }
}

enum Codes {
  SUCCESS,
  WRONG_ID,
  WRONG_LOCATION,
  INVALID_QR_CODE,
  ALREADY_VERIFIED,
  NEW_BUSINESS,
  EXCEEDED_VERIFICATION_CYCLE,
  INVALID_DETAILS,
}

class LocationsPage extends StatefulWidget {
  final String username;
  final String code;

  LocationsPage(this.username, {this.code});

  @override
  _LocationsPageState createState() => _LocationsPageState();
}

class _LocationsPageState extends State<LocationsPage> {
  PermissionGroup permission = PermissionGroup.location;
  bool _isLocation = false;
  bool _isGPS = false;
  bool _checkingLocation = false;
  bool _businessesReady = false;
  bool _loadingBusinesses = false;

  final Set<Marker> _markers = Set();
  MapType _currentMapType = MapType.normal;
  Completer<GoogleMapController> _controller = Completer();
  final Map<String, double> _markerColor = {
    '200': BitmapDescriptor.hueGreen,
    '400': BitmapDescriptor.hueRed,
    '403': BitmapDescriptor.hueViolet,
    '408': BitmapDescriptor.hueRose,
    '201': BitmapDescriptor.hueAzure,
    '202': BitmapDescriptor.hueMagenta,
    '203': BitmapDescriptor.hueYellow,
    '404': BitmapDescriptor.hueOrange,
  };
  String errorCode;

  // Set Treasury Square, Mombasa as center
  static const LatLng _center = const LatLng(-4.0636016, 39.6756078);

  void _setCode(String code, BuildContext context) {
//    LocationsScreen.restartApp(context, code);
    setState(() {
      errorCode = code;
      _load();
    });
  }

  Future<void> _selectCode(BuildContext context) async {
    switch (await showDialog(
      context: context,
      builder: (_) {
        return SimpleDialog(
          title: const Text('Select business type'),
          children: <Widget>[
            SimpleDialogOption(
              child: new Text(
                "Proper Business",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.SUCCESS);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "Incorrect Business ID",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.WRONG_ID);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "Incorrect Business Location",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.WRONG_LOCATION);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "Invalid QR Code",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.INVALID_QR_CODE);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "Business Permit Already Verified",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.ALREADY_VERIFIED);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "New Registered Business",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.NEW_BUSINESS);
              },
            ),
            SimpleDialogOption(
              child: new Text(
                "Exceeded Verification Cycle",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.pop(context, Codes.EXCEEDED_VERIFICATION_CYCLE);
              },
            ),
          ],
        );
      },
    )) {
      case Codes.SUCCESS:
        _setCode("200", context);
        break;
      case Codes.WRONG_ID:
        _setCode("400", context);
        break;
      case Codes.WRONG_LOCATION:
        _setCode("403", context);
        break;
      case Codes.INVALID_QR_CODE:
        _setCode("408", context);
        break;
      case Codes.ALREADY_VERIFIED:
        _setCode("201", context);
        break;
      case Codes.NEW_BUSINESS:
        _setCode("202", context);
        break;
      case Codes.EXCEEDED_VERIFICATION_CYCLE:
        _setCode("203", context);
        break;
    }
  }

  @override
  initState() {
    super.initState();
    errorCode = "200";
    checkPermission(permission).then((res) {
      if (res) {
        setState(() => _isLocation = true);
        checkServiceStatus(permission).then((serviceStatus) {
          if (serviceStatus.toString().contains("ServiceStatus.enabled")) {
            setState(() => _isGPS = true);
          } else {
            setState(() => _isGPS = false);
          }
        });
      } else {
        setState(() => _checkingLocation = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingLocation = false);
          getPermissionStatus(permission).then((result) {
            print("permission status is ${result.toString()}");
            if (result) {
              setState(() => _isLocation = true);
              checkServiceStatus(permission).then((serviceStatus) {
                if (serviceStatus
                    .toString()
                    .contains("ServiceStatus.enabled")) {
                  setState(() => _isGPS = true);
                } else {
                  setState(() => _isGPS = false);
                }
              });
            } else
              setState(() => _isLocation = false);
          });
        });
      }
    });
    _load();
  }

  _load() {
    _loadBusiness().then((results) {
      setState(() => _loadingBusinesses = false);
      if (results is String)
        showToast(results);
      else {
        setState(() => _markers.clear());
        setState(
          () => _markers.add(
            Marker(
              markerId: MarkerId("Treasury Square, Mombasa"),
              position: _center,
              infoWindow: InfoWindow(title: "Treasury Square, Mombasa"),
              icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueCyan,
              ),
            ),
          ),
        );
        for (var i = 0; i < results.length; i++) {
          setState(() {
            String errorcode = results[i]["errorcode"];
            if (errorcode == errorCode) {
              _markers.add(
                Marker(
                  markerId: MarkerId(results[i]["business_id"]),
                  position: LatLng(
                    double.tryParse(results[i]["latitude"]),
                    double.tryParse(results[i]["longitude"]),
                  ),
                  infoWindow: InfoWindow(
                    title: results[i]["business_name"],
                    snippet:
                        '${results[i]["physical_address"]} ${results[i]["plot_number"]}',
                  ),
                  icon: BitmapDescriptor.defaultMarkerWithHue(
//                    results['verified'] ? BitmapDescriptor.hueGreen : BitmapDescriptor.hueRed,
                    _markerColor[errorcode],
                  ),
                ),
              );
            }
          });
        }
        setState(() => _businessesReady = true);
      }
    }).catchError((err) {
      print('ERROR: ${err.toString()}');
      showToast("Something went wrong. Try again later");
      setState(() {
        _businessesReady = false;
        _loadingBusinesses = false;
      });
    });
  }

  // Get all businesses and their locations
  Future<dynamic> _loadBusiness() async {
    setState(() => _loadingBusinesses = true);
    final User user = await DBProvider.db.getUser(widget.username);
    final device = await deviceId;
    final api = await apiKey;
    final response = await http.get(
      await getAllBusinesses(),
      headers: {
        'token': user.token,
        'device_id': device,
        'api_key': api,
      },
    );
    if (response.statusCode == 200) {
      List results = json.decode(response.body);
      return results;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  /// method that is called on map creation and takes a MapController as a parameter
  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  /// vary view of map
  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: AppTitle(),
        ),
        body: Container(
          child: _isLocation
              ? !_businessesReady
                  ? _loadingBusinesses
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Center(child: errorResult("Businesses not available"))
                  : Stack(
                      children: <Widget>[
                        GoogleMap(
                          // manages camera function (position, animation, zoom)
                          onMapCreated: _onMapCreated,
                          // required parameter that sets the starting camera position.
                          // Camera position describes which part of the world you want the map to point at.
                          initialCameraPosition: CameraPosition(
                            target: _center,
                            zoom: 11.0,
                          ),
                          // set view of map
                          mapType: _currentMapType,
                          // set map's markers to highlight businesses
                          markers: _markers,
                          scrollGesturesEnabled: true,
                          myLocationEnabled: true,
                          zoomGesturesEnabled: true,
                          tiltGesturesEnabled: true,
                          rotateGesturesEnabled: true,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: FloatingActionButton(
                              onPressed: _onMapTypeButtonPressed,
                              materialTapTargetSize:
                                  MaterialTapTargetSize.padded,
                              backgroundColor: Colors.green,
                              child: const Icon(Icons.map, size: 26.0),
                              heroTag: "Layout",
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: FloatingActionButton(
                              onPressed: () => _selectCode(context),
                              heroTag: "Filter",
                              materialTapTargetSize:
                                  MaterialTapTargetSize.padded,
                              backgroundColor: Colors.green,
                              child: const Icon(Icons.autorenew),
                            ),
                          ),
                        ),
                      ],
                    )
              : Center(
                  child: _checkingLocation
                      ? Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 10.0)),
                            CircularProgressIndicator(),
                            Padding(
                                padding: EdgeInsets.symmetric(vertical: 20.0)),
                            Text(
                              "Looking for GPS and/or location",
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        )
                      : Column(
                          children: [
                            errorResult(
                              "Oops! Location permission not granted or GPS off",
                            ),
                            !_isGPS
                                ? RaisedButton(
                                    padding: const EdgeInsets.all(8.0),
                                    textColor: Colors.white,
                                    color: Colors.blue,
                                    onPressed: () {
                                      Theme.of(context).platform ==
                                              TargetPlatform.android
                                          ? openSettingsMenu(
                                              "ACTION_LOCATION_SOURCE_SETTINGS")
                                          : showToast(
                                              "Facility not implemented for platform");
                                    },
                                    child: new Text("Turn GPS on"),
                                  )
                                : Container()
                          ],
                        ),
                ),
        ),
      ),
      onWillPop: () => _goBack(context),
    );
  }

  _goBack(BuildContext context) {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (_) => LandingPage(widget.username)));
  }
}
