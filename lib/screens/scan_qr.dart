/// Scan business QR code here

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'scan_camera.dart';
import 'package:jiji_manage1/others/extras.dart';
import 'package:jiji_manage1/others/settings.dart';
import 'package:jiji_manage1/components/scan_button.dart';

class QRScanScreen extends StatefulWidget {
  final String username;

  QRScanScreen(this.username);

  @override
  _QRScanScreenState createState() => _QRScanScreenState();
}

class _QRScanScreenState extends State<QRScanScreen> {
  bool isCamera;

  PermissionGroup permission = PermissionGroup.location;
  bool _isLocation = false;
  bool _checkingLocation = false;
  bool _isGPS = false;

  String id;

  void _id(String str) => setState(() => id = str);

  @override
  void initState() {
    super.initState();
    isCamera = false;
    id = '';
    checkPermission(permission).then((res) {
      if (res) {
        setState(() => _isLocation = true);
        checkServiceStatus(permission).then((serviceStatus) {
          if (serviceStatus.toString().contains("ServiceStatus.enabled")) {
            setState(() => _isGPS = true);
          } else {
            setState(() => _isGPS = false);
          }
        });
      } else {
        setState(() => _checkingLocation = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingLocation = false);
          getPermissionStatus(permission).then((result) {
            print("permission status is " + result.toString());
            if (result) {
              setState(() => _isLocation = true);
              checkServiceStatus(permission).then((serviceStatus) {
                if (serviceStatus
                    .toString()
                    .contains("ServiceStatus.enabled")) {
                  setState(() => _isGPS = true);
                } else {
                  setState(() => _isGPS = false);
                }
              });
            } else
              setState(() => _isLocation = false);
          });
        });
      }
    });
  }

  void _onPressed() {
    if (!isCamera) {
      load().then((_) {
        setState(() => isCamera = !isCamera);
      });
    } else {
      setState(() => isCamera = !isCamera);
    }
  }

  void _enterID(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          actions: <Widget>[
            FlatButton(
              child: Text(
                'CONTINUE',
                style: TextStyle(
                  color: Colors.green[900],
                  fontSize: 17.5,
                ),
              ),
              onPressed: () {
                if (isStringEmpty(id))
                  showToast("Enter Business ID");
                else {
                  Navigator.of(context).pop();
                  _onPressed();
                }
              },
            ),
          ],
          title: Text(
            'Enter Business ID',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
            ),
          ),
          content: SingleChildScrollView(
            child: TextField(
              decoration: InputDecoration(
                hintText: "Business ID",
              ),
              onChanged: _id,
              onSubmitted: _id,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return !_isLocation
        ? Center(
            child: _checkingLocation
                ? Column(
                    children: [
                      Padding(padding: EdgeInsets.only(top: 10.0)),
                      CircularProgressIndicator(),
                      Padding(padding: EdgeInsets.symmetric(vertical: 20.0)),
                      Text(
                        "Looking for GPS and/or location",
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  )
                : someResult(),
          )
        : Stack(
            children: <Widget>[
              isCamera
                  ? CameraApp(username: widget.username, businessID: id)
                  : Center(
                      child: GestureDetector(
                        child: Icon(
                          FontAwesomeIcons.qrcode,
                          size: 150.0,
                        ),
                        onTap: () => _enterID(context),
                        onDoubleTap: () => _enterID(context),
                        onLongPress: () => showToast('Tap to show camera'),
                      ),
                    ),
              Positioned(
                bottom: 15.0,
                left: MediaQuery.of(context).size.width / 2 - 50,
                child: ScanButton(
                  onPressed: isCamera ? _onPressed : () => _enterID(context),
                  text: "${isCamera ? 'Finish' : 'Scan'}",
                ),
              ),
            ],
          );
  }

  Widget someResult() {
    return Column(
      children: [
        errorResult(
          "Oops! Location permission not granted or GPS off",
        ),
        !_isGPS
            ? RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  Theme.of(context).platform == TargetPlatform.android
                      ? openSettingsMenu("ACTION_LOCATION_SOURCE_SETTINGS")
                      : showToast("Facility not implemented for platform");
                },
                child: new Text("Turn GPS on"),
              )
            : Container()
      ],
    );
  }
}
