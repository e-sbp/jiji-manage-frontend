/// Here, we generate the widget to be seen on screen
/// and to be printed upon successful QR code verification

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'package:jiji_manage1/others/extras.dart';

class SuccessResult extends StatelessWidget {
  final Map<String, dynamic> response;
  final String user;

  SuccessResult(this.response, this.user);

  /// Used to parse time received from server

  DateTime parserReceivedTime() {
    final String verificationDate = response["date_verified"];
    String yr2hr = verificationDate.substring(0, 8);
    String min2ms = verificationDate.substring(8);
    return DateTime.parse('$yr2hr $min2ms');
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        // Receipt Header
        Padding(padding: EdgeInsets.symmetric(vertical: 3.0)),
        Container(
          child: Text(
            'Verification Receipt',
            style: TextStyle(color: Colors.black, fontSize: 24.0),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 0.0)),
        Container(
          child: Row(
            children: <Widget>[
              Container(
                width: 35.0,
                height: 35.0,
                decoration: new BoxDecoration(
                  border: Border.all(color: Colors.white, width: 1.5),
                  shape: BoxShape.rectangle,
                  image: new DecorationImage(
//                  fit: BoxFit.fill,
                    image: AssetImage("assets/images/ic_barcode.jpg"),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
              Text(
                'Mombasa County e-SBP',
                style: TextStyle(color: Colors.black, fontSize: 20.0),
              ),
            ],
          ),
          padding: EdgeInsets.only(left: 18.0),
        ),
        // Business Details
        Padding(padding: EdgeInsets.symmetric(vertical: 5.0)),
        Container(
          child: Text(
            'Name:\t ${response["business_name"]}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'ID:\t ${response["business_id"]}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'Plot:\t ${response["plot_number"]}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'Contact:\t ${response["mssdn"]}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'Location:\t ${response["physical_address"]}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'Valid for:\t ${response["grace_period"]} days',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          child: Text(
            'Next Verification Date: ${DateFormat('dd MMM yyyy').format(
              parserReceivedTime().add(
                Duration(
                  days: int.tryParse(response["grace_period"]),
                ),
              ),
            )}',
            style: TextStyle(color: Colors.black, fontSize: 18.0),
          ),
          padding: EdgeInsets.only(left: 10.0),
        ),
        // Receipt Footer
        Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 80.0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  child: Column(
                    children: [
                      SizedBox(height: 5.0),
                      Text(
                        'Verified by: $user',
                        style: TextStyle(color: Colors.black, fontSize: 16.0),
                      ),
                      SizedBox(height: 2.0),
                      Text(
                        '\tOn: ${DateFormat('dd MMM yyyy hh:mm:ss a').format(parserReceivedTime())}',
                        style: TextStyle(color: Colors.black, fontSize: 14.0),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.only(top: 15.0),
                ),
                flex: 2,
              ),
              SizedBox(width: 2.0),
              Expanded(
                child: Padding(
                  child: QrImage(
                    data: "${response["qrcode"] ?? ""}|${response["date_verified"] ?? ""}|${response["grace_period"] ?? ""}|" +
                        "${response["permit_id"] ?? ""}|${response["business_id"] ?? ""}|${response["date_registered"] ?? ""}",
                    gapless: false,
                    onError: (dynamic ex) {
                      print('[QR] ERROR - $ex');
                      showToast('Error! Maybe your input value is too long?');
                    },
                    size: 200.0,
                  ),
                  padding: EdgeInsets.only(left: 10.0),
                ),
                flex: 1,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
