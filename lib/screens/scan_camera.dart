/// Set up camera widget that will scan QR and barcode

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:camera/camera.dart';
import 'package:torch/torch.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
//import 'package:barcode_scan/barcode_scan.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

import '../others/urls.dart';
import '../others/utils.dart';
import '../others/settings.dart';
import '../others/extras.dart';
import '../database/database.dart';
import '../database/models.dart';
import 'flag_qr.dart';
import 'receipt.dart';

List<CameraDescription> cameras;

Future<Null> load() async {
  // Fetch the available cameras before initializing the camera.
  try {
    cameras = await availableCameras();
  } catch (e) {
    logError(e.code, e.description);
  }
}

class CameraApp extends StatefulWidget {
  final Function buttonCallback;
  final String username;
  final String businessID;

  CameraApp({this.buttonCallback, this.username, this.businessID});

  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp>
    with SingleTickerProviderStateMixin {
  CameraController controller;
  AnimationController animationController;
  Animation<double> verticalPosition;

  PermissionGroup permission = PermissionGroup.camera;
  bool _isCamera = false;
  bool _checkingCamera = false;

  String qrCode = '';
  String businessID;
  Map<String, dynamic> response = {};
  List<Barcode> _scanResults = <Barcode>[];
  bool _isDetecting = false;
  bool _isDetected = false;
  String errorMessage = '';
  String errorCode = '';
  bool _isCompleted = false;
  int _index = 0;
  List<int> flag = [400, 408];

  bool _hasFlash = false;
  bool _isOn = false;

  GlobalKey scanResultContainer = GlobalKey();
  Image scanPrintOut;

  // This provides a bridge to communicate with native code eg for location
  static const platform = const MethodChannel('flutter.native/helper');

  /// Upon successful QRCode detection,
  /// be it from native or Flutter code,
  /// the code and required business details like ID and location
  /// are processed to confirm if they are non-empty etc
  /// Upon successful processing, they are sent to the backend
  /// and the response received is then processed.
  /// If verification was successful, printing will commence,
  /// else business will be flagged as invalid and/or error rendered to screen
  void scanResult(String qrResult) {
    setState(() {
      _isDetected = true;
      qrCode = qrResult;
      try {
        // pause tracking
        controlTracking(false);
        getLocation().then((locationData) {
          if (locationData != null) {
            handleVerification(
              '${locationData.latitude}',
              '${locationData.longitude}',
            ).then((res) {
              if (res is String) {
                errorMessage = res;
                logError('ERROR', errorMessage);
              } else {
                errorCode = res['errorCode'];
                errorCode = errorCode == null ? res['errorcode'] : errorCode;
                if (int.tryParse(errorCode) == 200) {
                  print("RESPONSE: 200 detected");
                  response = res;
                  _isCompleted = true;
                } else {
                  errorMessage = res['desc'];
                  errorCode = res["errorCode"];
                  logError("${res["errorCode"]}", "${res['desc']}");
                  _isCompleted = false;
                }
                // restart tracking
                controlTracking(true);
              }
            });
          } else {
            errorMessage = "No location found. Please try again";
            _isCompleted = false;
          }
        }).catchError((err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
          _isCompleted = false;
        });
      } catch (err) {
        errorMessage = "Something went wrong. Please try again later";
        _isCompleted = false;
        print("$err");
      }
    });
  }

  // Calling native scanner
  Future _scanQR() async {
    showToast("Still building this");
//    qrCode = '';
//    try {
//      String qrResult = await BarcodeScanner.scan();
//      scanResult(qrResult);
//    } on PlatformException catch (ex) {
//      if (ex.code == BarcodeScanner.CameraAccessDenied) {
//        setState(() {
//          qrCode = '';
//          errorMessage = "Please grant camera permission to scan QR code";
//          _isDetected = true;
//        });
//      } else {
//        setState(() {
//          qrCode = '';
//          errorMessage = "Something went wrong. Try again later";
//          logError(ex.code, ex.message);
//          _isDetected = true;
//        });
//      }
//    } on FormatException {
//      setState(() {
//        qrCode = '';
//        errorMessage = "You pressed the back button before scanning anything";
//        _isDetected = true;
//      });
//    } catch (ex) {
//      setState(() {
//        qrCode = '';
//        errorMessage = "Something went wrong. Try again later";
//        logError(ex.code, ex.message);
//        _isDetected = true;
//      });
//    }
  }

  /// Here, we pause location tracking during scanning and verification
  /// and resume after verification
  Future<void> controlTracking(bool isTrack) async {
    String response = '';
    String method = isTrack ? "restartTracking" : "stopTracking";
    try {
      final String result = await platform.invokeMethod(method);
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }

    print(response);
  }

  Future<dynamic> handleVerification(String latitude, String longitude) async {
    if (isStringEmpty(qrCode)) return Future.value("Error. No code received");
    if (isStringEmpty(businessID))
      return Future.value("Error. No business ID received");
    final User user = await DBProvider.db.getUser(widget.username);
    final response = await http.get(
      await verify(qrCode, businessID, latitude, longitude),
      headers: {
        'token': user.token,
        'device_id': await deviceId,
        'api_key': await apiKey,
      },
    );
    if (response.statusCode == 200) {
      Map res;
      var resp = json.decode(response.body);
      if (resp is Map)
        res = resp['Response'][0];
      else if (resp is List)
        res = res = resp[0];
      else
        res = {
          'desc': "Server returned no result.\nPlease contact administrator",
          "errorCode": "503"
        };
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  @override
  void initState() {
    super.initState();
    _index = 0;
    businessID = widget.businessID ?? '';
    checkPermission(permission).then((res) {
      if (res) {
        setState(() {
          _isCamera = true;
          load();
          _initializeCamera();
        });
      } else {
        setState(() => _checkingCamera = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingCamera = false);
          getPermissionStatus(permission).then((result) {
            if (result)
              setState(() {
                _isCamera = true;
                load();
                _initializeCamera();
              });
            else {
              setState(() => _isCamera = false);
            }
          });
        });
      }
    });

    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 3),
    );

    animationController.addListener(() {
      this.setState(() {});
    });
    animationController.forward();
    verticalPosition = Tween<double>(begin: 0.0, end: 300.0).animate(
        CurvedAnimation(parent: animationController, curve: Curves.linear))
      ..addStatusListener((state) {
        if (state == AnimationStatus.completed) {
          animationController.reverse();
        } else if (state == AnimationStatus.dismissed) {
          animationController.forward();
        }
      });
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    bool hasFlash = false;
    try {
      hasFlash = await Torch.hasFlash;
      print("Device has flash ? $hasFlash");
      setState(() {
        _hasFlash = hasFlash;
      });
    } on PlatformException {
      print('Failed to see if has flash.');
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _hasFlash = hasFlash;
    });
  }

  /// If the device has available camera flash, user can switch it on and off here
  Future _turnFlash() async {
    _isOn ? Torch.flashOff : Torch.flashOn;
    var f = await Torch.hasFlash;
    setState(() {
      _hasFlash = f;
      _isOn = !_isOn;
    });
  }

  /// This function sets up the camera readily available
  /// and prepares and starts the QRCode detection process
  void _initializeCamera() async {
    await load();
    controller = CameraController(
      cameras[0],
      defaultTargetPlatform == TargetPlatform.iOS
          ? ResolutionPreset.low
          : ResolutionPreset.medium,
    );

    await controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });

    controller.startImageStream((CameraImage image) {
      if (_isDetecting) return;

      final FirebaseVision mlVision = FirebaseVision.instance;

      HandleDetection _handleDetection =
          mlVision.barcodeDetector().detectInImage;

      detect(image, _handleDetection).then(
        (dynamic result) {
          setState(() {
            _scanResults = result;
            _isDetecting = true;
          });

          _isDetecting = false;
        },
      ).catchError(
        (_) {
          _isDetecting = false;
        },
      );
    });
  }

  /// This function allows user to alternate
  /// between front, back and even external camera
  void _changeCamera() {
    setState(() {
      _index = (_index + 1) % cameras.length;
      try {
        onNewCameraSelected(cameras[_index]);
      } catch (err) {
        print("${err.toString()}");
      }
    });
  }

  /// Here, we set up a newly selected camera
  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      defaultTargetPlatform == TargetPlatform.iOS
          ? ResolutionPreset.low
          : ResolutionPreset.medium,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showToast('Camera error ${controller.value.errorDescription}');
      }
    });

    await controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });

    controller.startImageStream((CameraImage image) {
      if (_isDetecting) return;

      final FirebaseVision mlVision = FirebaseVision.instance;

      HandleDetection _handleDetection =
          mlVision.barcodeDetector().detectInImage;

      detect(image, _handleDetection).then(
        (dynamic result) {
          setState(() {
            _scanResults = result;
            _isDetecting = true;
          });

          _isDetecting = false;
        },
      ).catchError(
        (_) {
          _isDetecting = false;
        },
      );
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    animationController?.dispose();
    Torch.flashDispose;
    super.dispose();
  }

  /// This widget will be rendered if the scan result was successful
  /// From here, we shall print receipt
  Widget successResult() {
    if (response != null && response.isNotEmpty && scanPrintOut == null)
      Future.delayed(Duration(seconds: 5), _capturePng);
    return scanPrintOut == null
        ? Stack(
            children: [
              RepaintBoundary(
                key: scanResultContainer,
                child: SuccessResult(response, widget.username),
              ),
              Positioned(
                child: RaisedButton(
                  onPressed: _capturePng,
                  color: Colors.green,
                  child: Text(
                    'Printing...',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
                top: MediaQuery.of(context).size.height / 2 - 100,
                right: 0,
              ),
            ],
          )
        : scanPrintOut;
  }

  DateTime parserReceivedTime(Map<String, dynamic> response) {
    if (isStringEmpty(response["date_verified"])) return null;
    final String verificationDate = response["date_verified"];
    String yr2hr = verificationDate.substring(0, 8);
    String min2ms = verificationDate.substring(8);
    return DateTime.parse('$yr2hr $min2ms');
  }

  /// This function 'screenshots' the current screen,
  /// which is the one for scan success,
  /// saves it, then renders it on screen
  /// while initiating printing of the saved image
  Future<dynamic> _capturePng() async {
    try {
      // paint image from widget
      RenderRepaintBoundary boundary =
          scanResultContainer.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();

      // save file
      var dir = await getExternalStorageDirectory();
      var tempDir =
          await Directory('${dir.path}/pictures').create(recursive: true);
      final file = await new File(
              '${tempDir.path}/${DateTime.now().toUtc().toIso8601String()}.png')
          .create()
        ..writeAsBytesSync(pngBytes);

      // build data to be printed
      String data = "Verification Receipt\n" +
          "\tMombasa County e-SBP\n\n" +
          "Name:\t ${response["business_name"]}\n" +
          "ID:\t ${response["business_id"]}\n" +
          "Plot:\t ${response["plot_number"]}\n" +
          "Contact:\t ${response["mssdn"]}\n" +
          "Location:\t ${response["physical_address"]}\n" +
          "Valid for:\t ${response["grace_period"]} days\n" +
          "Next Verification Date: ${DateFormat('dd MMM yyyy').format(
            parserReceivedTime(response).add(
              Duration(
                days: int.tryParse(response["grace_period"]),
              ),
            ),
          )}\n\n" +
          "\tVerified by: ${widget.username}\n" +
          "\t\tOn: ${DateFormat('dd MMM yyyy hh:mm:ss a').format(parserReceivedTime(response))}";

      // pass saved image file path to printer
      printReceipt(data);

      // set saved image to screen
      setState(() => scanPrintOut = Image.file(file));
    } catch (e) {
      print(e);
    }
  }

  /// This widget, which is just a button, is rendered
  /// if scan fails due to incorrect business ID, QRCode or Location.
  /// Clicking the button takes one to screen for recording details of the business
  /// whose scan has failed
  Widget flagQR() {
    return RaisedButton(
      onPressed: () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) {
            return FlagQRScreen(qrCode, errorCode, widget.username);
          },
        ),
      ),
      child: Text('Continue'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return !_isCamera
        ? Center(
            child: _checkingCamera
                ? CircularProgressIndicator()
                : errorResult("Oops! Camera permission not granted."),
          )
        : Stack(
            children: <Widget>[
              new Container(
                child: new Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: _isDetected && _isCompleted
                        ? successResult()
                        : _isDetected
                            ? !isStringEmpty(errorMessage)
                                ? flag.contains(int.tryParse(errorCode))
                                    ? Row(
                                        children: <Widget>[
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(top: 10.0),
                                              child: flagQR()),
                                        ],
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                      )
                                    : Center(child: errorResult(errorMessage))
                                : Center(child: CircularProgressIndicator())
                            : Center(child: _cameraPreviewWidget())),
              ),
              Center(
                child: Stack(
                  children: <Widget>[
                    SizedBox(
                      height: 300.0,
                      width: 300.0,
                      child: Container(
                        decoration: BoxDecoration(
                          border: !_isDetected
                              ? Border.all(color: Colors.red, width: 2.0)
                              : null,
                        ),
                      ),
                    ),
                    Positioned(
                      top: verticalPosition.value,
                      child: Container(
                        width: 300.0,
                        height: 2.0,
                        color: _isDetected ? null : Colors.red,
                      ),
                    )
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  widget.buttonCallback == null
                      ? _buildResult(_scanResults)
                      : _buildRes(_scanResults),
                ],
              ),
            ],
          );
  }

  /// This widget is rendered as camera is looking for QRCode
  /// Can be used alternatively to [_buildResult]
  /// but is to be used only for QRCode registration purpose
  /// as the continue will complete process of capturing business details
  /// and allow user to send business details to backend
  /// if both QRCode and all details are sucessfully captured
  Widget _buildRes(List<Barcode> barcodes) {
    return Padding(
      child: Center(
        child: RaisedButton(
          child: Text(
            '${barcodes.length == 0 ? "Nothing found" : 'Continue'}',
            style: TextStyle(
              fontSize: 16.0,
              color: Colors.white,
            ),
          ),
          onPressed: barcodes.length == 0
              ? null
              : () => widget.buttonCallback(
                    barcodes[0].rawValue,
                    false,
                  ),
          color: Colors.green,
        ),
      ),
      padding:
          EdgeInsets.only(top: MediaQuery.of(context).size.height / 2 - 100),
    );
  }

  /// This widget is rendered as camera is looking for QRCode
  /// Can be used alternatively to [_buildResult]
  /// but is to be used only for business and QRCode verification purpose
  Widget _buildResult(List<Barcode> barcodes) {
    if (barcodes.length != 0 && isStringEmpty(qrCode))
      scanResult(barcodes[0].rawValue);
    return Expanded(
      flex: 1,
      child: barcodes.length == 0
          ? controller == null || !controller.value.isInitialized || _isDetected
              ? _buildTextRow("")
              : _buildTextRow("No barcode detected")
          : Container(),
    );
  }

  Widget _buildTextRow(text) {
    return Center(
      child: Text(text, style: Theme.of(context).textTheme.title),
    );
  }

  /// Display the preview from the camera
  /// (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    return controller == null || !controller.value.isInitialized
        ? const Text(
            'No camera selected',
            style: const TextStyle(
              color: Colors.black,
              fontSize: 24.0,
              fontWeight: FontWeight.w900,
            ),
          )
        : AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: Stack(
              children: [
                CameraPreview(controller),
                Positioned(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(left: 70.0)),
                            IconButton(
                              onPressed: _changeCamera,
                              icon: Icon(Icons.camera_alt),
                              color: Colors.white,
                            ),
                            Padding(padding: EdgeInsets.only(left: 45.0)),
                            IconButton(
                              onPressed: !_hasFlash
                                  ? () {
                                      showToast("Device does not have flash");
                                    }
                                  : _turnFlash,
                              icon: Icon(
                                  _isOn ? Icons.flash_off : Icons.flash_on),
                              color: Colors.white,
                            ),
                            Padding(padding: EdgeInsets.only(left: 45.0)),
                            IconButton(
                              onPressed: _scanQR,
                              icon: Icon(Icons.cancel),
                              color: Colors.white,
                            ),
                          ],
                        ),
                        width: MediaQuery.of(context).size.width - 20,
                        margin: EdgeInsets.only(left: 15.0),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                      ),
                    ],
                  ),
                  top: 0.0,
                  right: 10.0,
                ),
              ],
            ),
          );
  }
}
