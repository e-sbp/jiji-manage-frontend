/// Register business QR code here

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../others/urls.dart';
import '../others/extras.dart';
import '../others/settings.dart';
import '../components/scan_button.dart';
import '../database/models.dart';
import '../database/database.dart';
import 'scan_camera.dart';
import 'landing.dart';

class QRRegisterScreen extends StatefulWidget {
  final String username;

  QRRegisterScreen(this.username);

  @override
  _QRRegisterScreenState createState() => _QRRegisterScreenState();
}

class _QRRegisterScreenState extends State<QRRegisterScreen> {
  String scanRes;
  String bizID = '', bizName = '', plotNum = '', emailAddress = '';
  String physicalAddress = '', phoneNumber = '';
  bool isCamera = false;

  PermissionGroup permission = PermissionGroup.location;
  bool _isLocation = false;
  bool _checkingLocation = false;
  bool _isGPS = false;

  TextEditingController idController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController numController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  void _bizID(String str) => setState(() => bizID = str.trim());

  void _bizName(String str) => setState(() => bizName = str.trim());

  void _plotNum(String str) => setState(() => plotNum = str.trim());

  void _emailAddress(String str) => setState(() => emailAddress = str.trim());

  void _physicalAddress(String str) =>
      setState(() => physicalAddress = str.trim());

  void _phone(String str) => setState(() => phoneNumber = str.trim());

  void _setCamera(String qr, bool state) => setState(() {
        isCamera = state;
        scanRes = qr;
        idController.text = bizID ?? '';
        numController.text = plotNum ?? '';
        nameController.text = bizName ?? '';
        emailController.text = emailAddress ?? '';
        addressController.text = physicalAddress ?? '';
        phoneController.text = phoneNumber ?? '';
      });

  @override
  void initState() {
    scanRes = '';
    super.initState();
    checkPermission(permission).then((res) {
      if (res) {
        setState(() => _isLocation = true);
        checkServiceStatus(permission).then((serviceStatus) {
          if (serviceStatus.toString().contains("ServiceStatus.enabled")) {
            setState(() => _isGPS = true);
          } else {
            setState(() => _isGPS = false);
          }
        });
      } else {
        setState(() => _checkingLocation = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingLocation = false);
          getPermissionStatus(permission).then((result) {
            print("permission status is " + result.toString());
            if (result) {
              setState(() => _isLocation = true);
              checkServiceStatus(permission).then((serviceStatus) {
                if (serviceStatus
                    .toString()
                    .contains("ServiceStatus.enabled")) {
                  setState(() => _isGPS = true);
                } else {
                  setState(() => _isGPS = false);
                }
              });
            } else
              setState(() => _isLocation = false);
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return !_isLocation
        ? SingleChildScrollView(
            child: _checkingLocation
                ? Column(
                    children: [
                      Padding(padding: EdgeInsets.only(top: 10.0)),
                      CircularProgressIndicator(),
                      Padding(padding: EdgeInsets.symmetric(vertical: 20.0)),
                      Text(
                        "Looking for GPS and/or location",
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  )
                : someResult(),
          )
        : SingleChildScrollView(
            child: isCamera
                ? CameraApp(
                    buttonCallback: _setCamera, username: widget.username)
                : Stack(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading:
                                  const Icon(Icons.label, color: Colors.black),
                              title: TextField(
                                controller: idController,
                                decoration: InputDecoration(
                                  hintText: "Business ID",
                                ),
                                onChanged: _bizID,
                              ),
                            ),
                            Padding(
                              child: Divider(
                                height: 1.0,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                            ListTile(
                              leading: const Icon(FontAwesomeIcons.addressCard,
                                  color: Colors.black),
                              title: TextField(
                                controller: nameController,
                                decoration: InputDecoration(
                                  hintText: "Business Name",
                                ),
                                onChanged: _bizName,
                              ),
                            ),
                            Padding(
                              child: Divider(
                                height: 1.0,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                            ListTile(
                              leading: const Icon(FontAwesomeIcons.mapMarkerAlt,
                                  color: Colors.black),
                              title: TextField(
                                controller: numController,
                                decoration: InputDecoration(
                                  hintText: "Plot Number",
                                ),
                                onChanged: _plotNum,
                              ),
                            ),
                            Padding(
                              child: Divider(
                                height: 1.0,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                            ListTile(
                              leading: const Icon(Icons.local_post_office,
                                  color: Colors.black),
                              title: TextField(
                                controller: emailController,
                                decoration: InputDecoration(
                                  hintText: "Email Address",
                                ),
                                onChanged: _emailAddress,
                                keyboardType: TextInputType.emailAddress,
                              ),
                            ),
                            Padding(
                              child: Divider(
                                height: 1.0,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                            ListTile(
                              leading: const Icon(Icons.business,
                                  color: Colors.black),
                              title: TextField(
                                controller: addressController,
                                decoration: InputDecoration(
                                  hintText: "Physical Address",
                                ),
                                onChanged: _physicalAddress,
                              ),
                            ),
                            Padding(
                              child: Divider(
                                height: 1.0,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                            ListTile(
                              leading:
                                  const Icon(Icons.phone, color: Colors.black),
                              title: TextField(
                                controller: phoneController,
                                decoration: InputDecoration(
                                  hintText: "Phone Number",
                                ),
                                keyboardType: TextInputType.phone,
                                onChanged: _phone,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 15.0),
                            ),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.center,
                        ),
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 20),
                      ),
                      Positioned(
                        bottom: MediaQuery.of(context).size.height / 2 - 100,
                        right: 10.0,
                        child: isStringEmpty(scanRes)
                            ? ScanButton(
                                onPressed: /*_scanQR*/ () =>
                                    _setCamera(scanRes, true),
                                text: "Scan",
                              )
                            : ScanButton(
                                onPressed: _registerBiz,
                                bgColor: Colors.green,
                                text: "Register",
                                icon: Icons.send,
                              ),
                      ),
                    ],
                  ),
          );
  }

  Future<dynamic> makeRequest(Map<String, String> value) async {
    final User user = await DBProvider.db.getUser(widget.username);
    var response = await http.post(
      Uri.encodeFull(await registerQR()),
      headers: {
        "content-type": "application/json",
        'token': user.token,
        'device_id': await deviceId,
        'api_key': await apiKey,
      },
      body: json.encode(value),
    );
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _registerBiz() {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(scanRes))
        showToast("No code scanned");
      else if (isStringEmpty(bizID))
        showToast("Please enter the business ID");
      else if (isStringEmpty(bizName))
        showToast("Please enter the business name");
      else if (isStringEmpty(plotNum))
        showToast("Please enter the business' plot number");
      else if (isStringEmpty(emailAddress))
        showToast("Please enter the business' email address");
      else if (!validateEmail(emailAddress))
        showToast("Please enter a valid email address");
      else if (isStringEmpty(physicalAddress))
        showToast("Please enter the business' physical address");
      else if (isStringEmpty(phoneNumber))
        showToast("Please enter the business' phone number");
      else {
        showToast("Please wait...");
        try {
          getLocation().then((locationData) {
            if (locationData != null) {
              makeRequest({
                "business_id": bizID,
                "business_name": bizName,
                "qrcode": scanRes,
                "plot_number": plotNum,
                "mail_address": emailAddress,
                "physical_address": physicalAddress,
                "mssdn": phoneNumber,
                "latitude": '${locationData.latitude}',
                "longitude": '${locationData.longitude}',
                'status': "202",
              }).then((res) {
                final errorCode = res['errorCode'];
                final desc = res['desc'];
                if (desc == null || errorCode == null) {
                  showToast("Error while registering in");
                  return;
                }
                showToast("$desc");
                print("$errorCode");
                if (int.parse(errorCode) == 200)
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (_) => LandingPage(widget.username),
                    ),
                  );
              }).catchError((err) {
                showToast("Something went wrong. Please try again later");
                print("$err");
              });
            } else {
              showToast("No location found. Please try again");
            }
          }).catchError((err) {
            showToast("Something went wrong. Please try again later");
            print("$err");
          });
        } catch (err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        }
      }
    });
  }

  Widget someResult() {
    return Column(
      children: [
        errorResult(
          "Oops! Location permission not granted or GPS off",
        ),
        !_isGPS
            ? RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  Theme.of(context).platform == TargetPlatform.android
                      ? openSettingsMenu("ACTION_LOCATION_SOURCE_SETTINGS")
                      : showToast("Facility not implemented for platform");
                },
                child: new Text("Turn GPS on"),
              )
            : Container()
      ],
      mainAxisAlignment: MainAxisAlignment.start,
    );
  }
}
