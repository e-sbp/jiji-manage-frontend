/// Welcome page
/// Should show only when user installs app

import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'package:jiji_manage1/components/title.dart';
import 'package:jiji_manage1/database/database.dart';
import 'package:jiji_manage1/database/models.dart';
import 'package:jiji_manage1/others/colors.dart';
import 'package:jiji_manage1/others/extras.dart';
import 'package:jiji_manage1/others/urls.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTitle(),
      ),
      body: WelcomePage(),
    );
  }
}

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String userID = '', phoneNo = '';
  static const platform = const MethodChannel('flutter.native/helper');
  String _responseFromNativeCode = 'Waiting for Response...';

  Future<void> saveSBPDetails(String deviceId, String baseSBPUrl) async {
    String response = '';
    try {
      final String result = await platform.invokeMethod(
        "saveSBPDetails",
        {
          "device_id": deviceId,
          "base_url": baseSBPUrl,
          "api_key": await apiKey,
        },
      );
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }

    setState(() {
      _responseFromNativeCode = response;
    });

    print(_responseFromNativeCode);
  }

  void _id(String str) {
    if (!mounted) return;
    setState(() => userID = str.toLowerCase().trim());
  }

  void _phone(String str) {
    if (!mounted) return;
    setState(() => phoneNo = str.toLowerCase().trim());
  }

  Future<dynamic> handleWelcome(String userID, String phoneNo) async {
    String url = start(int.tryParse(userID), int.tryParse(phoneNo));
    try {
      final result = await InternetAddress.lookup('google.com' /*url*/);
      print(result);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await http.get(url);
        if (response.statusCode == 200) {
          Map resp = json.decode(response.body);
          Map res = resp['Response'][0];
          return res;
        } else {
          logError("ERROR!! ${response.statusCode}", "${response.body}");
          return "Something went wrong. Please try again later";
        }
      }
    } on SocketException catch (err) {
      logError('ERROR!! ${err.osError}', '${err.message}');
      return "ERROR!! Internet not connected";
    }
  }

  void _start(BuildContext context) {
    if (!mounted) return;
    DBProvider db = DBProvider.db;
    setState(() {
      if (isStringEmpty(userID))
        showToast("Please enter your user ID");
      else if (isStringEmpty(phoneNo))
        showToast("Please enter your phone number");
      else {
        showToast("Please wait...");
        try {
          handleWelcome(userID, phoneNo).then((res) async {
            if (res is String) {
              print(res);
              showToast(res);
              return;
            }
            final errorCode = res['errorCode'];
            if (int.tryParse(errorCode) == 200) {
              print('$res');
              CMI cmi = CMI.fromJson({
                "device_id": res["device_id"],
                "base_url_cmi": res["base_url_cmi"],
                "base_url_sbp": res["base_url_sbp"],
              });
              db.createCMIDetails(cmi).then((value) {
                showToast(res["desc"]);
                Navigator.of(context).pushReplacementNamed('/login');
              }).catchError((e) {
                print("Error starting app: $e");
                showToast("Unable to start. Please try again later");
              });
              saveSBPDetails(res["device_id"], res["base_url_sbp"]);
              String response = '';
              try {
                response =
                    await platform.invokeMethod("checkLocationPermission");
              } on PlatformException catch (e) {
                response = "Failed to Invoke: '${e.message}'.";
              }
              print(response);
            } else {
              logError("STARTING ERROR $errorCode", "Error: ${res['desc']}");
              showToast("${res['desc']}");
            }
          });
        } catch (err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        children: <Widget>[
          SizedBox(height: 20.0),
          Padding(
            child: Text(
              'Welcome to the Mombasa County e-SBP Management System',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 22.0,
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: 10.0),
          ),
          SizedBox(height: 10.0),
          Padding(
            child: Text(
              'To begin, Please enter your ID and phone number',
              style: TextStyle(
                color: Colors.black,
                fontStyle: FontStyle.italic,
                fontSize: 16.0,
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: 30.0),
          ),
          SizedBox(height: 20.0),
          Padding(
            child: AccentOverride(
              child: TextField(
                decoration: InputDecoration(
                  labelText: 'User ID',
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                onChanged: _id,
              ),
              color: myAccentColor,
            ),
            padding: EdgeInsets.symmetric(horizontal: 20.0),
          ),
          SizedBox(height: 12.0), // spacer
          // Password
          Padding(
            child: AccentOverride(
              child: TextField(
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  filled: true,
                ),
                keyboardType: TextInputType.phone,
                onChanged: _phone,
              ),
              color: myAccentColor,
            ),
            padding: EdgeInsets.symmetric(horizontal: 20.0),
          ),
          SizedBox(height: 12.0), // spacer
          // 294021442189
          Padding(
            child: FutureBuilder<String>(
              future: getDeviceID(),
              builder: (BuildContext buildContext, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    "Device ID: ${snapshot.data.toString()}",
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

                return CircularProgressIndicator();
              },
            ),
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
          ),
          // Places the buttons horizontally according to the padding
          ButtonBar(
            children: <Widget>[
              RaisedButton(
                child: Text(
                  'Continue',
                  style: TextStyle(
                      color: myBackgroundColor, fontWeight: FontWeight.bold),
                ),
                onPressed: () => _start(context),
                shape: StadiumBorder(),
                color: myPrimaryColor,
                elevation: 8.0,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<String> getDeviceID() async {
    String response = '';
    try {
      final String result = await platform.invokeMethod(
        "generateDeviceID",
      );
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }
    return response;
  }
}

class AccentOverride extends StatelessWidget {
  final Color color;
  final Widget child;

  const AccentOverride({Key key, this.color, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: child,
      data: Theme.of(context).copyWith(accentColor: color),
    );
  }
}
