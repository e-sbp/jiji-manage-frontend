/// First screen of app

import 'dart:async';
import 'package:flutter/material.dart';

import '../components/title.dart';
import '../database/database.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future checkFirstScreen() async {
    DBProvider dbProvider = DBProvider.db;
    List details = await dbProvider.getAllCMIDetails() ?? [];
    bool _seen = details.isNotEmpty ? true : false;

    if (_seen) {
      Navigator.of(context).pushReplacementNamed('/login');
    } else {
      Navigator.of(context).pushReplacementNamed('/welcome');
    }
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(milliseconds: 200), () {
      checkFirstScreen();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTitle(),
      ),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 20.0),
            Column(
              children: <Widget>[
                Image.asset(
                  'assets/images/ic_barcode.jpg',
                  scale: 1.5,
                ),
                SizedBox(height: 20.0),
                Material(
                  child: Text(
                    'E-SBP',
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Center(child: CircularProgressIndicator()),
          ],
        ),
      ),
    );
  }
}
