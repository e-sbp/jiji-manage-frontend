/// Let's us log in to the app from here

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

import '../database/database.dart';
import '../database/models.dart';

import 'landing.dart';
import '../others/extras.dart';
import '../others/colors.dart';
import '../others/urls.dart';
import '../others/settings.dart';
import '../components/title.dart';

// Stateless classes do not allow change in internal data
class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppTitle(),
      ),
      body: LoginPage(),
    );
  }
}

// Are immutable and have classes that allow change in internal data (state)
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

// State classes allow for change in internal data
class _LoginPageState extends State<LoginPage> {
  /// Keep track of number of times user has tapped back button.
  /// If >=2, exit the app
  int end = 0;
  String name = '', password = '';
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  /// Hold usernames that have been saved to app after successful login
  List<String> addedNames = List();

  /// Allow location permission checking
  PermissionGroup permission = PermissionGroup.location;
  bool _isLocation = false;
  bool _isGPS = false;
  bool _checkingLocation = false;

  void _name(String str) {
    if (!mounted) return;
    setState(() {
      name = str.toLowerCase().trim();
      end = 0;
    });
  }

  void _password(String str) {
    if (!mounted) return;
    setState(() {
      password = str;
      end = 0;
    });
  }

  /// Retrieve saved users and get their emails
  fetchSavedUsers() async {
    DBProvider dbProvider = DBProvider.db;
    dbProvider.getAllUsers().then((users) {
      for (int i = 0; i < users.length; i++)
        addedNames.add(users[i]['username']);
    }).catchError((err) {
      print("ERROR: $err");
    });
  }

  Future<dynamic> handleSignIn(String username, String password) async {
    DBProvider dbProvider = DBProvider.db;
    List details = await dbProvider.getAllCMIDetails() ?? [];
    if (details.isEmpty) return "Cannot login. No user saved.";
    final response = await http.get(
      await login(),
      headers: {
        'user_name': name,
        'pin': password,
        'api_key': await apiKey,
        'device_id': "${details[0]['device_id'] ?? ""}",
      },
    );
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _login(BuildContext context) {
    if (!mounted) return;
    DBProvider db = DBProvider.db;
    setState(() {
      end = 0;
      if (isStringEmpty(name))
        showToast("Please enter your username");
      else if (isStringEmpty(password))
        showToast("Please enter your password");
      else {
        showToast('Please wait...');
        try {
          handleSignIn(name, password).then((res) {
            final errorCode = res['errorCode'];
            final token = res['token'];
            String full = res['name'] ?? "";
            full = full.replaceAll(" ", "_");
            if (errorCode == null) {
              showToast("Error while logging in");
              logError("ERROR", "Error code missing");
              return;
            }
            if (int.tryParse(errorCode) == 200) {
              if (token == null) {
                showToast("Error while logging in");
                logError("ERROR", "Token missing");
                return;
              }
              db.getAllUsers().then((users) {
                List<String> names = [];
                for (var user in users) {
                  names.add(user["username"]);
                }
                // Here, we discover that the user trying to log in is not
                // the user allocated to this phone
                if (users.isNotEmpty && !names.contains(name)) {
                  // inform user this issue
                  showToast("This is not your designated device");
                  // then log him out
                  logOut(context, name);
                  return;
                } else {
                  db.getUser(name).then((User result) {
                    if (result != null) {
                      User user = result;
                      user.token = token;
                      db.updateUser(user).then((value) {
                        showToast('Successful login');
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (_) => LandingPage(name)));
                      }).catchError((e) {
                        print("Error logging in: $e");
                        showToast("Unable to login. Please try again later");
                      });
                    } else {
                      int uId = 0;
                      if (res['user_id'] is int)
                        uId = res['user_id'];
                      else if (res['user_id'] is String)
                        uId = int.tryParse(res['user_id']);
                      else
                        uId = 0;
                      User user = User.fromJson({
                        'user_id': uId,
                        'username': name,
                        'name': full,
                        'token': token,
                        'user_role': res['user_role'] ?? '',
                      });
                      db.createUser(user).then((value) {
                        showToast('Successful login');
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (_) => LandingPage(name)));
                      }).catchError((e) {
                        print("Error logging in: $e");
                        showToast("Unable to login. Please try again later");
                      });
                    }
                  });
                }
              });
            } else {
              logError("LOGIN ERROR", "Error: $errorCode");
              showToast("Failed login: ${res["desc"]}");
            }
          });
        } catch (err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        }
      }
    });
  }

  /// This method is called when the user is on login screen & presses the back button
  Future<bool> _confirmPop(BuildContext context) {
    if (end >= 1) {
      if (Theme.of(context).platform == TargetPlatform.android)
        SystemNavigator.pop();
      else if (Theme.of(context).platform == TargetPlatform.iOS)
        // exit(0);
        Navigator.pop(context);
      return Future.value(true);
    } else {
      end += 1;
      showToast('Press back again to leave app');
      return Future.value(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    /// registers a callback to veto attempts by the user to dismiss eg back press
    return WillPopScope(
      // Creates a widget that avoids operating system interfaces.
      // In this case, all subsequent children will have sufficient amount of padding
      child: SafeArea(
        // A scrollable list of widgets arranged linearly.
        child: !_isLocation
            ? Center(
                child: _checkingLocation
                    ? Column(
                        children: [
                          Padding(padding: EdgeInsets.only(top: 10.0)),
                          CircularProgressIndicator(),
                          Padding(
                              padding: EdgeInsets.symmetric(vertical: 20.0)),
                          Text(
                            "Looking for GPS and/or location",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          errorResult(
                            "Oops! Location permission not granted or GPS off",
                          ),
                          !_isGPS
                              ? RaisedButton(
                                  padding: const EdgeInsets.all(8.0),
                                  textColor: Colors.white,
                                  color: Colors.blue,
                                  onPressed: () {
                                    Theme.of(context).platform ==
                                            TargetPlatform.android
                                        ? openSettingsMenu(
                                            "ACTION_LOCATION_SOURCE_SETTINGS")
                                        : showToast(
                                            "Facility not implemented for platform");
                                  },
                                  child: new Text("Turn GPS on"),
                                )
                              : Container()
                        ],
                      ),
              )
            : ListView(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                children: <Widget>[
                  // Given a child, this widget forces child to have a specific width and/or height
                  // else it will adjust itself to given dimension
                  SizedBox(height: 20.0),
                  // display children in vertical array
                  Column(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/ic_barcode.jpg',
                        scale: 2.0,
                      ),
                      SizedBox(height: 10.0),
                      Material(
                        child: Text(
                          'E-SBP',
                          style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30.0),
                  // let user enter text
                  AccentOverride(
                    child: AutoCompleteTextField(
                      key: key,
                      suggestions: addedNames,
                      decoration: InputDecoration(
                        labelText: 'Username',
                        filled: true,
                      ),
                      style: TextStyle(color: Colors.black, fontSize: 16.0),
                      submitOnSuggestionTap: true,
                      clearOnSubmit: false,
                      textSubmitted: _name,
                      textChanged: _name,
                      itemBuilder: (context, item) {
                        return new Padding(
                            padding: EdgeInsets.all(8.0),
                            child: new Text(item));
                      },
                      itemSorter: (a, b) {
                        return a.compareTo(b);
                      },
                      onFocusChanged: (hasFocus) {},
                      itemFilter: (item, query) {
                        return item
                            .toLowerCase()
                            .startsWith(query.toLowerCase());
                      },
                      itemSubmitted: (item) => setState(() => name = item),
                    ),
                    color: myAccentColor,
                  ),
                  SizedBox(height: 7.0),
                  // spacer
                  // Password
                  AccentOverride(
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'Password',
                        filled: true,
                      ),
                      obscureText: true,
                      onChanged: _password,
                    ),
                    color: myAccentColor,
                  ),
                  // Places the buttons horizontally according to the padding
                  ButtonBar(
                    children: <Widget>[
                      RaisedButton(
                        child: Text(
                          'NEXT',
                          style: TextStyle(
                              color: myBackgroundColor,
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () => _login(context),
                        shape: StadiumBorder(),
                        color: myPrimaryColor,
                        elevation: 8.0,
                      ),
                    ],
                  ),
                ],
              ),
      ),
      // handle back press
      onWillPop: () => _confirmPop(context),
    );
  }

  @override
  void initState() {
    super.initState();
    fetchSavedUsers();
    checkLocation();
  }

  void checkLocation() {
    checkPermission(permission).then((res) {
      if (res) {
        setState(() => _isLocation = true);
        checkServiceStatus(permission).then((serviceStatus) {
          if (serviceStatus.toString().contains("ServiceStatus.enabled")) {
            setState(() => _isGPS = true);
          } else {
            setState(() => _isGPS = false);
          }
        });
      } else {
        setState(() => _checkingLocation = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingLocation = false);
          getPermissionStatus(permission).then((result) {
            print("permission status is ${result.toString()}");
            if (result) {
              setState(() => _isLocation = true);
              checkServiceStatus(permission).then((serviceStatus) {
                if (serviceStatus
                    .toString()
                    .contains("ServiceStatus.enabled")) {
                  setState(() => _isGPS = true);
                } else {
                  setState(() => _isGPS = false);
                }
              });
            } else
              setState(() => _isLocation = false);
          });
        });
      }
    });
  }
}

class AccentOverride extends StatelessWidget {
  final Color color;
  final Widget child;

  const AccentOverride({Key key, this.color, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      child: child,
      data: Theme.of(context).copyWith(accentColor: color),
    );
  }
}
