/// Here, we shall record details of business to flag
/// Has similar features as register QR code

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../components/scan_button.dart';
import '../database/database.dart';
import '../database/models.dart';
import '../others/settings.dart';
import '../others/extras.dart';
import '../components/title.dart';
import '../others/urls.dart';
import 'landing.dart';

class FlagQRScreen extends StatelessWidget {
  final String qrCode;
  final String errorCode;
  final String username;

  FlagQRScreen([this.qrCode, this.errorCode, this.username]);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: SingleChildScrollView(
          child: FlagQRPage(qrCode, errorCode, username),
        ),
        appBar: AppBar(
          title: AppTitle(),
        ),
      ),
      onWillPop: () => _goBack(context),
    );
  }

  _goBack(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (_) => LandingPage(username),
      ),
    );
  }
}

class FlagQRPage extends StatefulWidget {
  final String qrCode;
  final String errorCode;
  final String username;

  FlagQRPage([this.qrCode, this.errorCode, this.username]);

  _FlagQRPageState createState() => _FlagQRPageState();
}

class _FlagQRPageState extends State<FlagQRPage> {
  // business details
  String bizID = '', bizName = '', plotNum = '', emailAddress = '';
  String physicalAddress = '', qrCode = '', errorCode = '', phoneNumber = '';

  PermissionGroup permission = PermissionGroup.location;
  bool _isGPS = false;
  bool _isLocation = false;
  bool _checkingLocation = false;

  void _bizID(String str) => setState(() => bizID = str.trim());

  void _bizName(String str) => setState(() => bizName = str.trim());

  void _plotNum(String str) => setState(() => plotNum = str.trim());

  void _emailAddress(String str) => setState(() => emailAddress = str.trim());

  void _phone(String str) => setState(() => phoneNumber = str.trim());

  void _physicalAddress(String str) =>
      setState(() => physicalAddress = str.trim());

  Future<dynamic> makeRequest(Map<String, String> value) async {
    final User user = await DBProvider.db.getUser(widget.username);
    var response = await http.post(
      Uri.encodeFull(await flagQRCode()),
      headers: {
        "content-type": "application/json",
        'token': user.token,
        'device_id': await deviceId,
        'api_key': await apiKey,
      },
      body: json.encode(value),
    );
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  bool isDecimal(String str) {
    if (str == null) return false;
    return int.tryParse(str) != null;
  }

  void _registerFlaggedBiz() {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(qrCode))
        showToast("No code scanned");
      else if (isStringEmpty(errorCode))
        showToast("No status given");
      else if (isStringEmpty(bizID))
        showToast("Please enter the business ID");
      else if (isStringEmpty(bizName))
        showToast("Please enter the business name");
      else if (isStringEmpty(plotNum))
        showToast("Please enter the business' plot number");
      else if (isStringEmpty(emailAddress))
        showToast("Please enter the business' email address");
      else if (!validateEmail(emailAddress))
        showToast("Please enter a valid email address");
      else if (isStringEmpty(physicalAddress))
        showToast("Please enter the business' physical address");
      else if (isStringEmpty(phoneNumber))
        showToast("Please enter the business' phone number");
      else {
        showToast("Please wait...");
        try {
          getLocation().then((locationData) {
            if (locationData != null) {
              makeRequest({
                "business_id": bizID,
                "business_name": bizName,
                "qrcode": "$qrCode",
                "status": errorCode,
                "plot_number": plotNum,
                "mail_address": emailAddress,
                "physical_address": physicalAddress,
                "latitude": '${locationData.latitude}',
                "longitude": '${locationData.longitude}',
                "mssdn": phoneNumber,
              }).then((res) {
                final errorCode = res['errorCode'];
                final desc = res['desc'];
                if (desc == null || errorCode == null) {
                  showToast("Error while registering in");
                  return;
                }
                showToast("$desc");
                print("$errorCode");
                if (int.parse(errorCode) == 200)
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (_) => LandingPage(widget.username),
                    ),
                  );
              }).catchError((err) {
                showToast("Something went wrong. Please try again later");
                print("$err");
              });
            } else {
              showToast("No location found. Please try again");
            }
          }).catchError((err) {
            showToast("Something went wrong. Please try again later");
            print("$err");
          });
        } catch (err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        }
      }
    });
  }

  @override
  void initState() {
    qrCode = widget.qrCode;
    errorCode = widget.errorCode;
    super.initState();
    checkPermission(permission).then((res) {
      if (res) {
        setState(() => _isLocation = true);
        checkServiceStatus(permission).then((serviceStatus) {
          if (serviceStatus.toString().contains("ServiceStatus.enabled")) {
            setState(() => _isGPS = true);
          } else {
            setState(() => _isGPS = false);
          }
        });
      } else {
        setState(() => _checkingLocation = true);
        requestPermission(permission).then((val) {
          setState(() => _checkingLocation = false);
          getPermissionStatus(permission).then((result) {
            print("permission status is " + result.toString());
            if (result) {
              setState(() => _isLocation = true);
              checkServiceStatus(permission).then((serviceStatus) {
                if (serviceStatus
                    .toString()
                    .contains("ServiceStatus.enabled")) {
                  setState(() => _isGPS = true);
                } else {
                  setState(() => _isGPS = false);
                }
              });
            } else
              setState(() => _isLocation = false);
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return !_isLocation
        ? Center(
            child:
                _checkingLocation ? CircularProgressIndicator() : someResult())
        : Stack(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 2.0),
                    ),
                    ListTile(
                      leading: const Icon(Icons.label, color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Business ID",
                        ),
                        onChanged: _bizID,
                      ),
                    ),
                    Padding(
                      child: Divider(
                        height: 1.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    ListTile(
                      leading: const Icon(FontAwesomeIcons.addressCard,
                          color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Business Name",
                        ),
                        onChanged: _bizName,
                      ),
                    ),
                    Padding(
                      child: Divider(
                        height: 1.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    ListTile(
                      leading: const Icon(FontAwesomeIcons.mapMarkerAlt,
                          color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Plot Number",
                        ),
                        onChanged: _plotNum,
                      ),
                    ),
                    Padding(
                      child: Divider(
                        height: 1.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    ListTile(
                      leading: const Icon(Icons.local_post_office,
                          color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Email Address",
                        ),
                        onChanged: _emailAddress,
                        keyboardType: TextInputType.emailAddress,
                      ),
                    ),
                    Padding(
                      child: Divider(
                        height: 1.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    ListTile(
                      leading: const Icon(Icons.business, color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Physical Address",
                        ),
                        onChanged: _physicalAddress,
                      ),
                    ),
                    Padding(
                      child: Divider(
                        height: 1.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    ListTile(
                      leading: const Icon(Icons.phone, color: Colors.black),
                      title: TextField(
                        decoration: InputDecoration(
                          hintText: "Phone Number",
                        ),
                        keyboardType: TextInputType.phone,
                        onChanged: _phone,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 20),
              ),
              Positioned(
                bottom: MediaQuery.of(context).size.height / 2 - 100,
                right: 10.0,
                child: ScanButton(
                  onPressed: _registerFlaggedBiz,
                  bgColor: Colors.green,
                  text: "Continue",
                  icon: Icons.send,
                ),
              ),
            ],
          );
  }

  Widget someResult() {
    return Column(
      children: [
        errorResult(
          "Oops! Location permission not granted or GPS off",
        ),
        !_isGPS
            ? RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  Theme.of(context).platform == TargetPlatform.android
                      ? openSettingsMenu("ACTION_LOCATION_SOURCE_SETTINGS")
                      : showToast("Facility not implemented for platform");
                },
                child: new Text("Turn GPS on"),
              )
            : Container()
      ],
    );
  }
}
