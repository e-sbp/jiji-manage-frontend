/// Literally hold the title of the app that'll appear in AppBar

import 'package:flutter/material.dart';

class AppTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 18.0,
          height: 18.0,
          decoration: new BoxDecoration(
            border: Border.all(color: Colors.white, width: 1.5),
            shape: BoxShape.rectangle,
            image: new DecorationImage(
//                  fit: BoxFit.fill,
              image: AssetImage(
                "assets/images/ic_barcode.jpg",
              ),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
        Text(
          'Mombasa County e-SBP',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ),
      ],
    );
  }
}
