/// This is the button that is clicked to open the QR scanner

import 'package:flutter/material.dart';

class ScanButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final Color bgColor;
  final IconData icon;

  ScanButton(
      {@required this.onPressed, @required this.text, this.bgColor, this.icon});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      icon: Icon(icon == null ? Icons.camera_alt : icon),
      onPressed: onPressed,
      label: Text(text),
      backgroundColor: bgColor,
    );
  }
}
